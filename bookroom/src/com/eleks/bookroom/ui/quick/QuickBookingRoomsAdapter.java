package com.eleks.bookroom.ui.quick;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.eleks.bookroom.data.Constants;
import com.eleks.bookroom.data.structures.Room;
import com.eleks.bookroom.data.SetInterval;
import com.eleks.bookroom.R;
import com.eleks.bookroom.ui.advanced.AdvancedBookingFragment;
import com.eleks.bookroom.ui.UpdatedFragment;

import org.joda.time.DateTime;
import org.joda.time.Interval;

import java.util.List;
import static com.eleks.bookroom.data.Constants.*;

/**
 * Created by maryan.melnychuk on 09.12.13.
 */
public class QuickBookingRoomsAdapter extends BaseAdapter {
    private Bundle mData, mSharedData, mSelected;
    private List<Room> mRoomList;
    private Interval mActualInterval;
    private Interval[] mIntervals;
    private DateTime[] mBoundaryIntervals;
    private LayoutInflater mInflater;
    private FragmentActivity mActivity;
    private UpdatedFragment mUpdatedFragment;
    private Fragment mAdvancedFragment;

    private final int[] mPeriodsId = new int[]{R.id.period_text_1, R.id.period_text_2, R.id.period_text_3, R.id.period_text_4, R.id.period_text_5, R.id.period_text_6, R.id.period_text_7,
            R.id.period_text_8, R.id.period_text_9, R.id.period_text_10, R.id.period_text_11, R.id.period_text_12, R.id.period_text_13, R.id.period_text_14};
    private final int[] mStatusId = new int[]{R.id.period_state_1, R.id.period_state_2, R.id.period_state_3, R.id.period_state_4, R.id.period_state_5, R.id.period_state_6, R.id.period_state_7,
            R.id.period_state_8, R.id.period_state_9, R.id.period_state_10, R.id.period_state_11, R.id.period_state_12, R.id.period_state_13, };

    public QuickBookingRoomsAdapter(FragmentActivity activity, Bundle data, Bundle sharedData, UpdatedFragment updatedFragment) {
        mData = data;
        mSharedData = sharedData;
        mRoomList = sharedData.getParcelableArrayList(ROOM_LIST);
        mSelected = data.getBundle(SELECTED_DATA);
        mInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mUpdatedFragment = updatedFragment;
        mActivity = activity;
        mActualInterval = Room.getActualQuickInterval();
        mIntervals = Room.getActualQuickIntervals(mActualInterval);
        mBoundaryIntervals = Room.getActualBoundaryQuickIntervals(mIntervals);
    }

    @Override
    public int getCount() {
        return mRoomList.size();
    }

    @Override
    public Object getItem(int i) {
        return mRoomList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View contentView, ViewGroup parent) {
        View view = contentView;
        ViewHolder holder;
        if(view == null){
            view =  mInflater.inflate(R.layout.room_item, parent, false);
            holder = new ViewHolder();
            holder.textName = (TextView) view.findViewById(R.id.item_text);
            holder.periods = new TextView[Room.SIZE_OF_QUICK_INTERVALS + 1];
            holder.states = new View[Room.SIZE_OF_QUICK_INTERVALS];
            for (int i = 0; i < Room.SIZE_OF_QUICK_INTERVALS + 1; i++){
                holder.periods[i] = (TextView) view.findViewById(mPeriodsId[i]);
                if(i != Room.SIZE_OF_QUICK_INTERVALS){
                    holder.states[i] = view.findViewById(mStatusId[i]);
                }
            }
            holder.btnOk = (ImageButton) view.findViewById(R.id.btn_ok_booking);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        initializationView(position, holder);

        return view;
    }

    static class ViewHolder{
        public TextView textName;
        public ImageButton btnOk;
        public TextView[] periods;
        public View[] states;
    }

    private void initializationView(int position, ViewHolder holder){
        Room room = mRoomList.get(position);
        List<Boolean> roomStatuses = room.getQuickRoomStatus(mActualInterval);

        holder.textName.setText(room.getName());

        for (int i = 0; i < Room.SIZE_OF_QUICK_INTERVALS + 1; i++){
            if(mBoundaryIntervals[i].getMinuteOfHour() == 15 || mBoundaryIntervals[i].getMinuteOfHour() == 45){

            } else {
                holder.periods[i].setText(mBoundaryIntervals[i].toString(TIME_FORMAT));
            }
            if(i != Room.SIZE_OF_QUICK_INTERVALS){
                if(roomStatuses.get(i)){
                    holder.states[i].setBackgroundColor(mActivity.getResources().getColor(R.color.color_state_available));
                } else {
                    holder.states[i].setBackgroundColor(mActivity.getResources().getColor(R.color.color_state_not_available));
                }
            }
        }

        selectingInterval(room, holder.states, roomStatuses);

        setOkListener(room, holder, position);
    }

    private void selectingInterval(Room room, View[] states, List<Boolean> roomStatuses){
        if (mSelected == null){
            mSelected = new Bundle();
        }
        if(mSelected.getBooleanArray(room.getName()) == null){
            boolean[] selectedPeriod = new boolean[Room.SIZE_OF_QUICK_INTERVALS];
            for (int i = 0; i < Room.SIZE_OF_QUICK_INTERVALS; i++){
                selectedPeriod[i] = false;
            }
            mSelected.putBooleanArray(room.getName(), selectedPeriod);
        }

        mData.putBundle(Constants.SELECTED_DATA, mSelected);

        boolean[] selectedItemPeriod = mSelected.getBooleanArray(room.getName());

        int leftBoundary = -1;
        int rightBoundary = -1;
        for (int i = 0; i <= Room.SIZE_OF_QUICK_INTERVALS; i++){
            if(i != Room.SIZE_OF_QUICK_INTERVALS){
                if(leftBoundary == -1) {
                    if(selectedItemPeriod[i]) {
                        states[i].setBackgroundColor(mActivity.getResources().getColor(R.color.color_item_boundary));
                        leftBoundary = i;
                    }
                } else if(rightBoundary == -1) {
                    if(selectedItemPeriod[i]){
                        states[i].setBackgroundColor(mActivity.getResources().getColor(R.color.color_state_selected));
                    } else {
                        states[i - 1].setBackgroundColor(mActivity.getResources().getColor(R.color.color_item_boundary));
                        rightBoundary = i - 1;
                    }
                }
            } else {
                if(selectedItemPeriod[i - 1]){
                    rightBoundary = i - 1;
                    states[i - 1].setBackgroundColor(mActivity.getResources().getColor(R.color.color_item_boundary));
                }
            }
            if(i != Room.SIZE_OF_QUICK_INTERVALS){
                SetInterval.setIntervalListener(i, states, roomStatuses, selectedItemPeriod, mActivity, Room.SIZE_OF_QUICK_INTERVALS);
            }
        }
    }

    private void setOkListener(final Room room, ViewHolder holder, final int position){
        holder.btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onOkPress(room, position);
            }
        });
    }

    public void onOkPress(Room room, int position){
        boolean[] selectedItemPeriod = mSelected.getBooleanArray(room.getName());
        boolean isSelected = false;
        DateTime startDate = null;
        DateTime endDate = null;
        boolean isEnd = false;
        for (int i = 0; i < Room.SIZE_OF_QUICK_INTERVALS; i++){
            if(startDate == null) {
                if(selectedItemPeriod[i]){
                    startDate = mBoundaryIntervals[i];
                    endDate = mBoundaryIntervals[i];
                    isSelected = true;
                }
            } else if (isSelected) {
                endDate = mBoundaryIntervals[i];
                if(selectedItemPeriod[i]){
                    isSelected = true;
                } else {
                    isSelected = false;
                }
            } else if(selectedItemPeriod[i]){
                Toast.makeText(QuickBookingRoomsAdapter.this.mActivity, R.string.error_wrong_interval, Toast.LENGTH_SHORT).show();
                return;
            }
        }
        if(endDate != null) {
            if(isEnd){
                endDate = mBoundaryIntervals[Room.SIZE_OF_QUICK_INTERVALS];
            }

            mAdvancedFragment = new AdvancedBookingFragment(mSharedData, position, startDate, endDate);
            mActivity.getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, mAdvancedFragment)
                    .commit();
        } else {
            Toast.makeText(QuickBookingRoomsAdapter.this.mActivity, R.string.error_interval_not_set, Toast.LENGTH_LONG).show();
        }
    }

    public boolean isAdvancedBookingVisible(){
        if(mAdvancedFragment != null){
            if(mAdvancedFragment.isVisible()){
                return true;
            }
        }
        return false;
    }
}

