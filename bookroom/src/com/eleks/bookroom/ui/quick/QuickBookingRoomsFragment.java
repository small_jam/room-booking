package com.eleks.bookroom.ui.quick;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.eleks.bookroom.data.Constants;
import com.eleks.bookroom.ui.advanced.AdvancedBookingFragment;
import com.eleks.bookroom.data.commands.GetRoomStatusCommand;
import com.eleks.bookroom.R;
import com.eleks.bookroom.ui.custom.components.RBListFragment;
import com.eleks.bookroom.data.structures.Room;

import org.joda.time.DateTime;
import org.joda.time.Interval;

import java.util.ArrayList;
import java.util.List;
import static com.eleks.bookroom.data.Constants.*;

/**
 * Created by maryan.melnychuk on 03.12.13.
 */
public class QuickBookingRoomsFragment extends RBListFragment {
    private Bundle mQuickData, mSharedData;
    private List<Room> mRoomsList;
    private QuickBookingRoomsAdapter mAdapter;
    private int mRequestId = -1;

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        ((ActionBarActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        if(getArguments() != null) {
            if(getArguments().getBundle(ADVANCED_FREE_BUSY_DATA) != null){
                if(getArguments().getBundle(ADVANCED_FREE_BUSY_DATA).getInt(TYPE_ADVANCED) == AdvancedBookingFragment.TYPE_QUICK){
                    AdvancedBookingFragment fragment = new AdvancedBookingFragment();
                    fragment.setArguments(getArguments());
                    fragment.setType(AdvancedBookingFragment.TYPE_QUICK);
                    getActivity().getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.container, fragment)
                            .addToBackStack(null)
                            .commit();
                    return;
                }
            }
            mQuickData = getArguments().getBundle(QUICK_FREE_BUSY_DATA);
            mSharedData = getArguments().getBundle(SHARED_DATA);
            if(mSharedData != null){
                mRoomsList = mSharedData.getParcelableArrayList(ROOM_LIST);
            } else {
                mSharedData = new Bundle();
            }
            if(mQuickData != null){
                mAdapter = new QuickBookingRoomsAdapter(getActivity(), mQuickData, mSharedData, this);
                setListAdapter(mAdapter);
                setListShown(true);
                return;
            } else if(mRoomsList != null){
                if(mRoomsList.get(0).getDates().get(0).withTime(0,0,0,0).isEqual(DateTime.now().withTime(0,0,0,0))){
                    mQuickData = new Bundle();
                    mAdapter = new QuickBookingRoomsAdapter(getActivity(), mQuickData, mSharedData, this);
                    setListAdapter(mAdapter);
                    setListShown(true);
                    return;
                }
            }

        }
        getRoomStatus();
        setListShown(false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        ActivityCompat.invalidateOptionsMenu(getActivity());
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(mAdapter != null){
            if(!mAdapter.isAdvancedBookingVisible()){
                outState.putString(ADVANCED_FREE_BUSY_DATA, null);
            }
        }
        outState.putBundle(SHARED_DATA, mSharedData);
        outState.putBundle(QUICK_FREE_BUSY_DATA, mQuickData);
    }

    @Override
    public void updateFragment() {
        getRoomStatus();
        setListShown(false);
    }

    @Override
    public Bundle getSharedDate() {
        Bundle data = new Bundle();
        data.putBundle(SHARED_DATA, mSharedData);
        return data;
    }

    private void getRoomStatus(){
        Log.d(LOG_TAG, "getRoomStatus");
        DateTime startDate = DateTime.now().withTime(0,0,0,0);
        DateTime endDate = startDate.plusDays(2).minusSeconds(1);
        Interval interval = new Interval(startDate, endDate);
        mRequestId = getServiceHandler().getRoomStatus(interval);
    }

    @Override
    public void onServiceCallback(int requestId, Intent requestIntent, int resultCode, Bundle data) {
        Log.d(LOG_TAG, "onServiceCallback");
        if(getServiceHandler().check(requestIntent, GetRoomStatusCommand.class)){
            Log.d(LOG_TAG, "right onServiceCallback");
            if(resultCode == GetRoomStatusCommand.RESPONSE_SUCCESS){
                if(mQuickData == null){
                    mQuickData = new Bundle();
                }
                if(mSharedData == null){
                    mSharedData = new Bundle();
                }
                mRoomsList = data.getParcelableArrayList(ROOM_LIST);
                mSharedData.putParcelableArrayList(ROOM_LIST, (ArrayList)mRoomsList);
                mAdapter = new QuickBookingRoomsAdapter(QuickBookingRoomsFragment.this.getActivity(), mQuickData, mSharedData, QuickBookingRoomsFragment.this);
                setListAdapter(mAdapter);
                setListShown(true);
            } else if(resultCode == GetRoomStatusCommand.RESPONSE_FAILED){
                if(Constants.isNetworkOnline(getActivity())){
                    Toast.makeText(getActivity(), R.string.error_connection, Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getActivity(), R.string.error_no_connection, Toast.LENGTH_LONG).show();
                }
            }
        }
    }
}
