package com.eleks.bookroom.ui.main;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;

import com.bugsense.trace.BugSenseHandler;
import com.eleks.bookroom.data.Constants;
import com.eleks.bookroom.ui.UpdatedFragment;
import com.eleks.bookroom.ui.advanced.AdvancedBookingFragment;
import com.eleks.bookroom.ui.booked.MyBookedFragment;
import com.eleks.bookroom.ui.quick.QuickBookingRoomsFragment;
import com.eleks.bookroom.R;
import com.eleks.bookroom.ui.login.LoginActivity;

public class MainActivity extends ActionBarActivity implements ActionBar.OnNavigationListener {

    private static final String STATE_SELECTED_NAVIGATION_ITEM = "selected_navigation_item";
    private Bundle mFragmentData;
    private Fragment mFragment;
    private int mDuration;

    
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //bugsense integration
        if(Constants.IS_CRASH_ACTIVE){
            BugSenseHandler.initAndStartSession(this, Constants.BUGSENSE_ID);
        }

        // Set up the action bar to show a dropdown list.
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);

        // Set up the dropdown list navigation in the action bar.
        actionBar.setListNavigationCallbacks(
                // Specify a SpinnerAdapter to populate the dropdown list.
                new ArrayAdapter<String>(
                        actionBar.getThemedContext(),
                        android.R.layout.simple_list_item_1,
                        android.R.id.text1,
                        new String[] {
                                getString(R.string.title_section_quick_booking),
                                getString(R.string.title_section_advanced_booking),
                                getString(R.string.title_section_my_booked)
                        }),
                this);

        if(savedInstanceState != null){
            mDuration = savedInstanceState.getInt(Constants.CURRENT_DURATION);
        } else {
            mDuration = Constants.DURATION_DAY;
        }

    }

    
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore the previously serialized current dropdown position.
        if (savedInstanceState.containsKey(STATE_SELECTED_NAVIGATION_ITEM)) {
            getSupportActionBar().setSelectedNavigationItem(
                    savedInstanceState.getInt(STATE_SELECTED_NAVIGATION_ITEM));

        }
        mFragmentData = savedInstanceState;
    }

    @Override
    public boolean onSupportNavigateUp() {
        UpdatedFragment currentFragment = (UpdatedFragment)getSupportFragmentManager().findFragmentById(R.id.container);
        Bundle sharedData = currentFragment.getSharedDate();
        if(currentFragment instanceof AdvancedBookingFragment){
            Fragment fragment;
            switch (((AdvancedBookingFragment)currentFragment).getType()){
                case AdvancedBookingFragment.TYPE_EDIT:
                    fragment = new MyBookedFragment(mDuration);
                    fragment.setArguments(sharedData);
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.container, fragment)
                            .commit();
                    break;
                case AdvancedBookingFragment.TYPE_QUICK:
                    fragment = new QuickBookingRoomsFragment();
                    fragment.setArguments(sharedData);
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.container, fragment)
                            .commit();
                    break;
            }
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        UpdatedFragment currentFragment = (UpdatedFragment)getSupportFragmentManager().findFragmentById(R.id.container);
        Bundle sharedData = currentFragment.getSharedDate();
        if(currentFragment instanceof AdvancedBookingFragment){
            Fragment fragment;
            switch (((AdvancedBookingFragment)currentFragment).getType()){
                case AdvancedBookingFragment.TYPE_EDIT:
                    fragment = new MyBookedFragment(mDuration);
                    fragment.setArguments(sharedData);
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.container, fragment)
                            .commit();
                    return;
                case AdvancedBookingFragment.TYPE_QUICK:
                    fragment = new QuickBookingRoomsFragment();
                    fragment.setArguments(sharedData);
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.container, fragment)
                            .commit();
                    return;
            }
        }
        super.onBackPressed();
    }


    public void onSaveInstanceState(Bundle outState) {
        // Serialize the current dropdown position.
        outState.putInt(STATE_SELECTED_NAVIGATION_ITEM, getSupportActionBar().getSelectedNavigationIndex());
        if(mDuration != 0){
            outState.putInt(Constants.CURRENT_DURATION, mDuration);
        }
        getSupportFragmentManager().findFragmentById(R.id.container).onSaveInstanceState(outState);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        menu.clear();
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public int getDuration(){
        return mDuration;
    }

    
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_refresh:
                ((UpdatedFragment) getSupportFragmentManager().findFragmentById(R.id.container)).updateFragment();
                return true;

            case R.id.action_log_off:
                SharedPreferences preferences = getSharedPreferences(Constants.PREFERENCES, MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString(Constants.PREFERENCES_USERNAME, "");
                editor.putString(Constants.PREFERENCES_PASSWORD, "");
                editor.putString(Constants.PREFERENCES_DOMAIN, "");
                editor.putBoolean(Constants.IS_PREFERENCES_SET, false);
                editor.commit();
                Constants.sExchangeHandler = null;
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                finish();
                return true;

            case R.id.action_time_interval:
                BookingTimeIntervalDialog dialog = new BookingTimeIntervalDialog();
                dialog.show(getSupportFragmentManager(), "BookingTimeIntervalDialog");
                return true;
        }
        return false;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if(getSupportFragmentManager().findFragmentById(R.id.container) instanceof MyBookedFragment){
            menu.findItem(R.id.action_time_interval).setVisible(true);
        } else {
            menu.findItem(R.id.action_time_interval).setVisible(false);
        }
        return true;
    }

    public boolean onNavigationItemSelected(int position, long id) {
        UpdatedFragment currentFragment = (UpdatedFragment)getSupportFragmentManager().findFragmentById(R.id.container);
        Bundle sharedData = null;
        if(currentFragment != null){
            sharedData = currentFragment.getSharedDate();
        }
        mFragment = FragmentFactory.newInstance(position, mDuration);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, mFragment)
                .commit();
        if(sharedData != null){
            mFragmentData = sharedData;
        }
        mFragment.setArguments(mFragmentData);
        return true;
    }

    /**
     * A create new mFragment.
     */
    public static class FragmentFactory {
        /**
         * Returns a new instance of mFragment for the given section
         * number.
         */
        public static Fragment newInstance(int sectionNumber, int duration) {
            Fragment fragment = null;
            switch(sectionNumber){
                case 0:
                    fragment = new QuickBookingRoomsFragment();
                    break;
                case 1:
                    fragment = new AdvancedBookingFragment();
                    break;
                case 2:
                    fragment = new MyBookedFragment(duration);
                    break;
            }
            return fragment;
        }
    }

    public class BookingTimeIntervalDialog extends DialogFragment {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(R.string.dialog_booking_time_interval_title);
            builder.setItems(R.array.searching_intervals, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int position) {
                    Fragment fragment1 = getSupportFragmentManager().findFragmentById(R.id.container);
                    if (fragment1 instanceof MyBookedFragment) {
                        mDuration = position + 1;
                        ((MyBookedFragment) fragment1).setDuration(mDuration);
                    }
                }
            });
            return builder.create();
        }
    }

}
