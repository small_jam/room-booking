package com.eleks.bookroom.ui.advanced;

import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.eleks.bookroom.data.Constants;
import com.eleks.bookroom.data.structures.Meeting;
import com.eleks.bookroom.R;
import com.eleks.bookroom.ui.UpdatedFragment;

import java.util.concurrent.ExecutionException;
import static com.eleks.bookroom.data.Constants.*;

/**
 * Created by Marek on 27.12.13.
 */
public class BookingConformationDialog extends DialogFragment implements View.OnClickListener{
    private Meeting mMeeting;
    private UpdatedFragment mUpdatedFragment;

    private TextView mTextTitle, mTextViewLocation, mTextViewTimeFrom, mTextViewTimeTo, mTextDate;
    private Button mBtnOk, mBtnCancel;

    public BookingConformationDialog(UpdatedFragment updatedFragment, Meeting meeting) {
        mMeeting = meeting;
        mUpdatedFragment = updatedFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.confirmation_dialog, parent);

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));

        mTextTitle = (TextView) view.findViewById(R.id.text_dialog_title);
        mTextViewLocation = (TextView) view.findViewById(R.id.dialog_confirmation_location);
        mTextViewTimeFrom = (TextView) view.findViewById(R.id.dialog_confirmation_from_time);
        mTextViewTimeTo = (TextView) view.findViewById(R.id.dialog_confirmation_to_time);
        mTextDate = (TextView) view.findViewById(R.id.dialog_confirmation_date);
        mBtnOk = (Button) view.findViewById(R.id.btn_dialog_ok);
        mBtnCancel = (Button) view.findViewById(R.id.btn_dialog_cancel);

        if(mMeeting.getUniqueId() != null){
            mTextTitle.setText(getActivity().getResources().getString(R.string.dialog_update_title));
        } else {
            mTextTitle.setText(getActivity().getResources().getString(R.string.dialog_send_title));
        }
        mTextViewLocation.setText(getActivity().getResources().getString(R.string.dialog_location) + " " + mMeeting.getLocation());
        mTextViewTimeFrom.setText(getActivity().getResources().getString(R.string.dialog_from) + " " + mMeeting.getMeetingInterval().getStart().toString(TIME_FORMAT));
        mTextViewTimeTo.setText(getActivity().getResources().getString(R.string.dialog_to) + " " + mMeeting.getMeetingInterval().getEnd().toString(TIME_FORMAT));

        mTextDate.setText(getActivity().getResources().getString(R.string.dialog_date) + " " + mMeeting.getMeetingInterval().getStart().toString(DATE_FORMAT));

        mBtnOk.setOnClickListener(this);
        mBtnCancel.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_dialog_ok:
                CreateMeetingRequestTask task = new CreateMeetingRequestTask();
                task.execute();
                try {
                    if(task.get()) {
                        mUpdatedFragment.updateFragment();
                        if(mMeeting.getUniqueId() == null){
                            Toast.makeText(getActivity(), R.string.meeting_request_created, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.meeting_request_update, Toast.LENGTH_SHORT).show();
                        }
                        mMeeting.getInvites().clear();
                    } else {
                        mMeeting.getInvites().clear();
                        if(mMeeting.getUniqueId() == null){
                            Toast.makeText(getActivity(), R.string.error_meeting_not_created, Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.error_meeting_not_updated, Toast.LENGTH_LONG).show();
                        }
                        if(!Constants.isNetworkOnline(getActivity())){
                            Toast.makeText(getActivity(), R.string.error_no_connection, Toast.LENGTH_LONG).show();
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } finally {
                    getDialog().dismiss();
                }
                break;
            case R.id.btn_dialog_cancel:
                getDialog().dismiss();
                break;
        }
    }

    class CreateMeetingRequestTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... voids) {
            mMeeting.getInvites().add(mMeeting.getLocationEmail());
            boolean state = false;
            if(mMeeting.getUniqueId() != null){
               state = sExchangeHandler.updateMeetingRequest(mMeeting);
            } else {
               state = sExchangeHandler.createMeetingRequest(mMeeting);
            }
            return state;
        }

    }
}
