package com.eleks.bookroom.ui.advanced;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FilterQueryProvider;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.eleks.bookroom.data.Constants;
import com.eleks.bookroom.data.databases.emails.EmailsDatabaseHelper;
import com.eleks.bookroom.data.commands.GetMeetingCommand;
import com.eleks.bookroom.data.commands.GetRoomStatusCommand;
import com.eleks.bookroom.data.structures.Meeting;
import com.eleks.bookroom.R;
import com.eleks.bookroom.data.commands.RBBaseCommand;
import com.eleks.bookroom.ui.UpdatedFragment;
import com.eleks.bookroom.ui.custom.components.RBFragment;
import com.eleks.bookroom.data.structures.Room;
import com.eleks.bookroom.data.SetInterval;
import com.eleks.bookroom.ui.main.MainActivity;
import com.eleks.bookroom.ui.booked.MyBookedFragment;
import com.eleks.bookroom.ui.quick.QuickBookingRoomsFragment;

import org.joda.time.DateTime;
import org.joda.time.Interval;

import java.util.ArrayList;
import java.util.List;
import static com.eleks.bookroom.data.Constants.*;

/**
 * Created by maryan.melnychuk on 19.12.13.
 */
public class AdvancedBookingFragment extends RBFragment implements View.OnClickListener {
    private final int[] mIntervalsIds = new int[]{R.id.period_advanced_state_1, R.id.period_advanced_state_2, R.id.period_advanced_state_3, R.id.period_advanced_state_4, R.id.period_advanced_state_5,
            R.id.period_advanced_state_6, R.id.period_advanced_state_7, R.id.period_advanced_state_8, R.id.period_advanced_state_9, R.id.period_advanced_state_10, R.id.period_advanced_state_11,
            R.id.period_advanced_state_12, R.id.period_advanced_state_13, R.id.period_advanced_state_14, R.id.period_advanced_state_15, R.id.period_advanced_state_16, R.id.period_advanced_state_17,
            R.id.period_advanced_state_18, R.id.period_advanced_state_19, R.id.period_advanced_state_20, R.id.period_advanced_state_21, R.id.period_advanced_state_22, R.id.period_advanced_state_23,
            R.id.period_advanced_state_24, R.id.period_advanced_state_25, R.id.period_advanced_state_26, R.id.period_advanced_state_27, R.id.period_advanced_state_28, R.id.period_advanced_state_29,
            R.id.period_advanced_state_30, R.id.period_advanced_state_31, R.id.period_advanced_state_32, R.id.period_advanced_state_33, R.id.period_advanced_state_34, R.id.period_advanced_state_35,
            R.id.period_advanced_state_36, R.id.period_advanced_state_37, R.id.period_advanced_state_38, R.id.period_advanced_state_39, R.id.period_advanced_state_40, R.id.period_advanced_state_41,
            R.id.period_advanced_state_42, R.id.period_advanced_state_43, R.id.period_advanced_state_44, R.id.period_advanced_state_45, R.id.period_advanced_state_46, R.id.period_advanced_state_47,
            R.id.period_advanced_state_48, R.id.period_advanced_state_49, R.id.period_advanced_state_50, R.id.period_advanced_state_51, R.id.period_advanced_state_52, R.id.period_advanced_state_53,
            R.id.period_advanced_state_54, R.id.period_advanced_state_55, R.id.period_advanced_state_56, R.id.period_advanced_state_57, R.id.period_advanced_state_58, R.id.period_advanced_state_59,
            R.id.period_advanced_state_60};
    private Bundle mAdvancedData, mSharedData;
    private LinearLayout mLayoutLoading, mLayoutSelectingContainer;
    private ToggleButton mToggleBtnSelecting;
    private View mView, mLayoutTimeline, mLayoutManual;
    private List<Room> mRooms;
    private int mActiveRoomPosition = -1;
    private Bundle mSelected;
    private Interval[] mIntervals;
    private DateTime[] mBoundaryIntervals;
    private Meeting mMeeting;
    private int mRequestId = -1;

    private Button mBtnSave, mBtnCancel, mBtnDate, mBtnAddInvite, mBtnFromTime, mBtnToTime;
    private Spinner mSpinnerRoms;
    private View[] mViewIntervalsState;
    private EditText mEditSubject, mEditMassageBody;
    private List<LinearLayout> mLayoutInvites;
    private List<AutoCompleteTextView> mAutoCompleteInvites;
    private List<ImageButton> mBtnInvites;
    private LinearLayout mLayout;
    private ArrayAdapter<String> mRoomNameAdapter;
    private EmailsDatabaseHelper mEmailsDatabaseHelper;
    private SQLiteDatabase mDb;
    private SimpleCursorAdapter mAdapter;
    private int mType;
    private boolean mIsFirst;
    public final static int TYPE_NORMAL = 0;
    public final static int TYPE_EDIT = 1;
    public final static int TYPE_QUICK = 2;

    public AdvancedBookingFragment(){
        mType = TYPE_NORMAL;
    }

    public AdvancedBookingFragment(Meeting meeting, Bundle sharedData){
        mMeeting = meeting;
        mType = TYPE_EDIT;
        mSharedData = sharedData;
        mRooms = mSharedData.getParcelableArrayList(ROOM_LIST);
    }

    public AdvancedBookingFragment(Bundle sharedData, int roomPosition, DateTime startDate, DateTime endDate){
        mType = TYPE_QUICK;
        mSharedData = sharedData;
        mRooms = mSharedData.getParcelableArrayList(ROOM_LIST);
        mActiveRoomPosition = roomPosition;
        mMeeting = new Meeting();
        mMeeting.setLocation(mRooms.get(roomPosition).getName());
        mMeeting.setLocationEmail(mRooms.get(roomPosition).getEmail());
        mMeeting.setMeetingInterval(new Interval(startDate, endDate));
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        if(getArguments() != null) {
            mAdvancedData = getArguments().getBundle(ADVANCED_FREE_BUSY_DATA);
            mSharedData = getArguments().getBundle(SHARED_DATA);
            if(mSharedData != null){
                mRooms = mSharedData.getParcelableArrayList(ROOM_LIST);
            } else {
                mSharedData = new Bundle();
            }
            if(mAdvancedData != null){
                mMeeting = mAdvancedData.getParcelable(MEETING);
                loadData();
                return;
            }
        }
        if(mAdvancedData == null){
            mAdvancedData = new Bundle();
        }
        if(mMeeting == null){
            mMeeting = new Meeting();
            mAdvancedData.putParcelable(MEETING, mMeeting);
        }
        if(mType == TYPE_NORMAL){
            mMeeting.setMeetingInterval(new Interval(DateTime.now().withSecondOfMinute(0), DateTime.now().withSecondOfMinute(0).plusHours(1)));
        }
        mIsFirst = true;

        if(mRooms == null || (mIsFirst && mType == TYPE_EDIT) || !mRooms.get(0).getDates().get(0).withTime(0,0,0,0).isEqual(mMeeting.getMeetingInterval().getStart().withTime(0,0,0,0))){
            getRoomStatus(false);
        } else {

            loadData();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        FrameLayout root = new FrameLayout(getActivity());

        mLayoutLoading = new LinearLayout(getActivity());
        mLayoutLoading.setOrientation(LinearLayout.VERTICAL);
        mLayoutLoading.setGravity(Gravity.CENTER);

        ProgressBar progress = new ProgressBar(getActivity(), null, android.R.attr.progressBarStyleLarge);
        mLayoutLoading.addView(progress, new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        root.addView(mLayoutLoading, new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        //
        mView = inflater.inflate(R.layout.advanced_booking_fragment, null);
        mView.setVisibility(View.GONE);
        root.addView(mView, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        mLayoutSelectingContainer = (LinearLayout)mView.findViewById(R.id.layout_type_time_selecting);
        mLayoutManual = inflater.inflate(R.layout.manual_selecting, null);
        mLayoutTimeline = inflater.inflate(R.layout.timeline_selecting, null);
        mToggleBtnSelecting = (ToggleButton) mView.findViewById(R.id.toggle_btn_manual_timeline_booking);
        mToggleBtnSelecting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setSelectingView();
            }
        });
        setSelectingView();
        mBtnFromTime = (Button) mLayoutManual.findViewById(R.id.btn_from_time);
        mBtnToTime = (Button) mLayoutManual.findViewById(R.id.btn_to_time);
        mBtnFromTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FromTimePickerDialog dialog = new FromTimePickerDialog();
                dialog.show(getActivity().getSupportFragmentManager(), "fromTime");
            }
        });
        mBtnToTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ToTimePickerDialog dialog = new ToTimePickerDialog();
                dialog.show(getActivity().getSupportFragmentManager(), "toTime");
            }
        });

        mBtnSave = (Button) mView.findViewById(R.id.btn_advanced_save);
        mBtnCancel = (Button) mView.findViewById(R.id.btn_advanced_cancel);
        mBtnDate = (Button) mView.findViewById(R.id.btn_advanced_date);
        mBtnAddInvite = (Button) mView.findViewById(R.id.btn_advanced_add_invite);
        mSpinnerRoms = (Spinner) mView.findViewById(R.id.spinner_advanced_room);
        mViewIntervalsState = new View[Room.SIZE_OF_ADVANCED_INTERVALS];
        for(int i = 0; i < Room.SIZE_OF_ADVANCED_INTERVALS; i++){
            mViewIntervalsState[i] = mLayoutTimeline.findViewById(mIntervalsIds[i]);
        }
        mLayout = (LinearLayout) mView.findViewById(R.id.layout_advanced_invite);
        mEditSubject = (EditText) mView.findViewById(R.id.edit_advanced_subject);
        mEditMassageBody = (EditText) mView.findViewById(R.id.edit_advanced_body);
        mAutoCompleteInvites = new ArrayList<AutoCompleteTextView>();
        mBtnInvites = new ArrayList<ImageButton>();
        mLayoutInvites = new ArrayList<LinearLayout>();
        mAutoCompleteInvites.add((AutoCompleteTextView) mView.findViewById(R.id.auto_complete_invite));

        createAdapter();
        mAutoCompleteInvites.get(0).setAdapter(mAdapter);

        mBtnInvites.add((ImageButton) mView.findViewById(R.id.btn_remove_invite));
        mLayoutInvites.add((LinearLayout) mView.findViewById(R.id.layout_invite));
        mView.findViewById(R.id.btn_remove_invite).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAutoCompleteInvites.remove(0);
                mBtnInvites.remove(0);
                ((LinearLayout) view.getParent().getParent()).removeView(mLayoutInvites.get(0));
                mLayoutInvites.remove(0);
            }
        });

        if(mType == TYPE_NORMAL){
            mBtnCancel.setVisibility(View.GONE);
            mBtnSave.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        } else {
            mBtnCancel.setOnClickListener(this);
        }

        mBtnSave.setOnClickListener(this);
        mBtnDate.setOnClickListener(this);
        mBtnAddInvite.setOnClickListener(this);

        root.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        return root;
    }

    private void setSelectingView() {
        if(mIsFirst && mType != TYPE_NORMAL){
            mToggleBtnSelecting.setChecked(true);
            mIsFirst = false;
        }
        mLayoutSelectingContainer.removeView(mLayoutManual);
        mLayoutSelectingContainer.removeView(mLayoutTimeline);
        if(mToggleBtnSelecting.isChecked()){
            mLayoutSelectingContainer.addView(mLayoutManual, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            mBtnFromTime.setText(mMeeting.getMeetingInterval().getStart().toString(TIME_FORMAT));
            mBtnToTime.setText(mMeeting.getMeetingInterval().getEnd().toString(TIME_FORMAT));
        } else {
            mLayoutSelectingContainer.addView(mLayoutTimeline, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        }
    }

    private void createAdapter() {
        mEmailsDatabaseHelper = new EmailsDatabaseHelper(getActivity());
        mDb = mEmailsDatabaseHelper.getReadableDatabase();
        mAdapter = new SimpleCursorAdapter(getActivity(), android.R.layout.simple_list_item_1, null, new String[]{EmailsDatabaseHelper.EMAIL_ROW},
                new int[]{android.R.id.text1});
        // This will provide the labels for the choices to be displayed in the AutoCompleteTextView
        mAdapter.setCursorToStringConverter(new SimpleCursorAdapter.CursorToStringConverter() {
            @Override
            public CharSequence convertToString(Cursor cursor) {
                final int colIndex = cursor.getColumnIndexOrThrow(EmailsDatabaseHelper.EMAIL_ROW);
                return cursor.getString(colIndex);
            }
        });

        // This will run a query to find the descriptions for a given vehicle.
        mAdapter.setFilterQueryProvider(new FilterQueryProvider() {
            @Override
            public Cursor runQuery(CharSequence description) {
                Cursor managedCursor = null;
                if(description != null){
                    managedCursor = mEmailsDatabaseHelper.getDescriptionsFor(description.toString(), mDb);
                }
                return managedCursor;
            }
        });
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        ActivityCompat.invalidateOptionsMenu(getActivity());
        if(mType > TYPE_NORMAL){
            ((ActionBarActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } else {
            ((ActionBarActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        saveData();

        outState.putBundle(SHARED_DATA, mSharedData);
        outState.putBundle(ADVANCED_FREE_BUSY_DATA, mAdvancedData);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mDb.close();
        mEmailsDatabaseHelper.close();
    }

    public String getUniqueId(){
        return mMeeting.getUniqueId();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mAdvancedData.putString(ADVANCED_FREE_BUSY_DATA, null);
    }

    public void setType(int type){
        mType = type;
    }

    @Override
    public void updateFragment() {
        mRooms = null;
        getRoomStatus(true);
    }

    private void loadData(){
        mBtnDate.setText(mMeeting.getMeetingInterval().getStart().toString(DATE_FORMAT));
        mBtnFromTime.setText(mMeeting.getMeetingInterval().getStart().toString(TIME_FORMAT));
        mBtnToTime.setText(mMeeting.getMeetingInterval().getEnd().toString(TIME_FORMAT));
        mEditSubject.setText(mMeeting.getSubject());
        mEditMassageBody.setText(mMeeting.getMessageOfBody());

        mToggleBtnSelecting.setChecked(mAdvancedData.getBoolean(Constants.SELECTED));

        setSelectingView();

        setNameSpinner();

        setIntervalViews();

        List<String> invites = mMeeting.getInvites();
        if(invites != null){
            if(mAutoCompleteInvites.get(0).getText().toString().equals("")){
                for(String invite : invites){
                    if(!mRooms.get(mActiveRoomPosition).getEmail().equalsIgnoreCase(invite)){
                        if(mAutoCompleteInvites.get(0).getText().toString().equals("")){
                            mAutoCompleteInvites.get(0).setText(invite);
                        } else {
                            onBtnAddInvite(invite);
                        }
                    }
                }
            }
        }

        mLayoutLoading.setVisibility(View.GONE);
        mView.setVisibility(View.VISIBLE);
    }

    private void saveData(){
        mAdvancedData.putInt(Constants.ACTIVE_ROOM, mActiveRoomPosition);

        if(mIsFirst && mType > TYPE_NORMAL){
            mAdvancedData.putBoolean(Constants.SELECTED, true);
            mIsFirst = false;
        } else {
            mAdvancedData.putBoolean(Constants.SELECTED, mToggleBtnSelecting.isChecked());
        }
        List<String> invites = mMeeting.getInvites();
        invites.clear();
        for(EditText editInvite : mAutoCompleteInvites){
            if(!editInvite.getText().toString().equals("") && !invites.contains(editInvite.getText().toString())){
                invites.add(editInvite.getText().toString());
            }
        }
        mMeeting.setSubject(mEditSubject.getText().toString());
        mMeeting.setMessageOfBody(mEditMassageBody.getText().toString());

        mAdvancedData.putInt(TYPE_ADVANCED, mType);

        mAdvancedData.putParcelable(MEETING, mMeeting);
    }

    public int getType(){
        return mType;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_advanced_save:
                onBtnSave();
                break;
            case R.id.btn_advanced_cancel:
                Fragment fragment = null;
                if(mType == TYPE_EDIT){
                    fragment = new MyBookedFragment(((MainActivity)getActivity()).getDuration());
                }
                if(mType == TYPE_QUICK){
                    fragment = new QuickBookingRoomsFragment();
                }
                Bundle data = new Bundle();
                data.putBundle(SHARED_DATA, mSharedData);
                fragment.setArguments(data);
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, fragment)
                        .commit();
                break;
            case R.id.btn_advanced_date:
                DatePickerFragment dataDialogFragment = new DatePickerFragment();
                dataDialogFragment.show(getActivity().getSupportFragmentManager(), "datePicker");
                break;
            case R.id.btn_advanced_add_invite:
                onBtnAddInvite("");
                break;
        }
    }

    private void onBtnSave(){
        if(mToggleBtnSelecting.isChecked()){
            showConformationDialog();
        } else {
            if(isIntervalRightSelected()){
                showConformationDialog();
            }
        }
    }

    private void showConformationDialog() {
        saveData();
        BookingConformationDialog conformationDialog = new BookingConformationDialog(this, mMeeting);
        conformationDialog.show(getActivity().getSupportFragmentManager(), "conformationDialog");
    }

    private boolean isIntervalRightSelected() {
        boolean[] selectedItemPeriod = mSelected.getBooleanArray(mMeeting.getLocation());
        boolean isSelected = false;
        DateTime startDate = null;
        DateTime endDate = null;
        boolean isEnd = false;
        for (int i = 0; i < Room.SIZE_OF_ADVANCED_INTERVALS; i++){
            if(i == Room.SIZE_OF_ADVANCED_INTERVALS - 1 && selectedItemPeriod[i]){
                isEnd = true;
            }
            if(startDate == null) {
                if(selectedItemPeriod[i]){
                    startDate = mBoundaryIntervals[i];
                    endDate = mBoundaryIntervals[i];
                    isSelected = true;
                }
            } else if (isSelected) {
                endDate = mBoundaryIntervals[i];
                if(selectedItemPeriod[i]){
                    isSelected = true;
                } else {
                    isSelected = false;
                }
            } else if(selectedItemPeriod[i]){
                Toast.makeText(getActivity(), R.string.error_wrong_interval, Toast.LENGTH_SHORT).show();
                return false;
            }
        }
        if(endDate != null) {
            if(isEnd){
                endDate = mBoundaryIntervals[Room.SIZE_OF_ADVANCED_INTERVALS];
            }

            if(DateTime.now().isAfter(startDate)){
                Toast.makeText(getActivity(), R.string.error_interval_before_current_time, Toast.LENGTH_LONG).show();
                return false;
            }
            mMeeting.setMeetingInterval(new Interval(startDate, endDate));
            return true;
        } else {
            Toast.makeText(getActivity(), R.string.error_interval_not_set, Toast.LENGTH_LONG).show();
            return false;
        }
    }

    private void onBtnAddInvite(String email){
        final LinearLayout layoutInvite = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.invite, null);
        mLayout.addView(layoutInvite);
        mLayoutInvites.add(layoutInvite);
        final AutoCompleteTextView autoCompleteInvite = (AutoCompleteTextView) layoutInvite.findViewById(R.id.auto_complete_invite);
        mAutoCompleteInvites.add(autoCompleteInvite);
        if(mAdapter != null){
            autoCompleteInvite.setAdapter(mAdapter);
        }

        if(!email.equals("")){
            autoCompleteInvite.setText(email);
        }
        ImageButton imageButton = (ImageButton) layoutInvite.findViewById(R.id.btn_remove_invite);
        mBtnInvites.add(imageButton);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int i = mBtnInvites.indexOf(view);
                mAutoCompleteInvites.remove(i);
                mBtnInvites.remove(i);
                mLayout.removeView(layoutInvite);
                mLayoutInvites.remove(i);
            }
        });
    }

    @Override
    public void onServiceCallback(int requestId, Intent requestIntent, int resultCode, Bundle data) {
        if(getServiceHandler().check(requestIntent, GetRoomStatusCommand.class)){
            if(resultCode == RBBaseCommand.RESPONSE_SUCCESS){
                mRooms = data.getParcelableArrayList(ROOM_LIST);
                if(mSharedData == null){
                    mSharedData = new Bundle();
                }
                mSharedData.putParcelableArrayList(ROOM_LIST, (ArrayList)mRooms);
                loadRooms();
            } else if(resultCode == RBBaseCommand.RESPONSE_FAILED){
                errorCallback();
            }
        } else if(getServiceHandler().check(requestIntent, GetMeetingCommand.class)){
            if(resultCode == RBBaseCommand.RESPONSE_SUCCESS){
                mMeeting = data.getParcelable(MEETING);
                mAdvancedData.putParcelable(MEETING, mMeeting);
                if(mRooms == null || !mRooms.get(0).getDates().get(0).withTime(0,0,0,0).isEqual(mMeeting.getMeetingInterval().getStart().withTime(0,0,0,0))){
                    DateTime startDate = mMeeting.getMeetingInterval().getStart().withTimeAtStartOfDay();
                    DateTime endDate = startDate.plusDays(2).minusMinutes(1);
                    mRequestId = getServiceHandler().getRoomStatus(new Interval(startDate, endDate));
                    Log.d(LOG_TAG, "IF");
                } else {
                    loadRooms();
                    Log.d(LOG_TAG, "ELSE");
                }
            } else if(resultCode == RBBaseCommand.RESPONSE_FAILED){
                errorCallback();
            }
        }
    }

    private void loadRooms(){
        for (Room room : mRooms){
            if(mMeeting.getInvites().contains(room.getEmail())){
                mMeeting.getInvites().remove(room.getEmail());
            }
        }

        loadData();

        if(mMeeting.getUniqueId() != null){
            mActiveRoomPosition = getRoomId();
        }
        if (mSelected == null){
            mSelected = new Bundle();
            mAdvancedData.putBundle(Constants.SELECTED_DATA, mSelected);
        }
    }

    private void errorCallback(){
        if(Constants.isNetworkOnline(getActivity())){
            Toast.makeText(getActivity(), R.string.error_connection, Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getActivity(), R.string.error_no_connection, Toast.LENGTH_LONG).show();
        }
    }

    private void getRoomStatus(boolean update) {
        mLayoutLoading.setVisibility(View.VISIBLE);
        mView.setVisibility(View.GONE);
        if(mMeeting.getMeetingInterval() == null){
            mMeeting.setMeetingInterval(new Interval(DateTime.now(), DateTime.now()));
        }

        if(mType != TYPE_EDIT || update){
            DateTime startTime = mMeeting.getMeetingInterval().getStart().withTime(0,0,0,0);
            DateTime endTime = startTime.plusDays(2).minusMinutes(1);
            mRequestId = getServiceHandler().getRoomStatus(new Interval(startTime, endTime));
        } else {
            mRequestId = getServiceHandler().getMeeting(mMeeting);
        }
    }

    private int getRoomId(){
        int roomSize = mRooms.size();
        for(int i = 0; i < roomSize; i++){
            if(mMeeting.getLocation().equals(mRooms.get(i).getName())){
                return i;
            }
        }
        return 0;
    }

    private void setNameSpinner(){
        List<String> roomNames = new ArrayList<String>();
        int roomsSize = mRooms.size();
        for(int i = 0; i < roomsSize; i++){
            roomNames.add(mRooms.get(i).getName());
        }
        if(mActiveRoomPosition == -1){
            mActiveRoomPosition = mAdvancedData.getInt(ACTIVE_ROOM, 0);
        }
        mRoomNameAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, roomNames);
        mRoomNameAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerRoms.setAdapter(mRoomNameAdapter);
        mSpinnerRoms.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                mActiveRoomPosition = pos;
                mMeeting.setLocation(mRooms.get(pos).getName());
                mMeeting.setLocationEmail(mRooms.get(pos).getEmail());
                setIntervalViews();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        mSpinnerRoms.setSelection(mActiveRoomPosition);
    }

    private void setIntervalViews(){
        Room room = mRooms.get(mActiveRoomPosition);
        List<Boolean> roomStatuses = room.getAdvancedRoomStatus(mMeeting.getMeetingInterval().getStart());
        mIntervals = Room.getActualAdvancedIntervals(mMeeting.getMeetingInterval().getStart());
        mBoundaryIntervals = Room.getActualBoundaryAdvancedIntervals(mIntervals);

        for (int i = 0; i < Room.SIZE_OF_ADVANCED_INTERVALS; i++){
            if(roomStatuses.get(i)){
                mViewIntervalsState[i].setBackgroundColor(getActivity().getResources().getColor(R.color.color_state_available));
            } else {
                mViewIntervalsState[i].setBackgroundColor(getActivity().getResources().getColor(R.color.color_state_not_available));
            }
        }
        mSelected = mAdvancedData.getBundle(Constants.SELECTED_DATA);
        selectingInterval(room, mViewIntervalsState, roomStatuses);
    }

    private void selectingInterval(Room room, View[] states, List<Boolean> roomStatuses){
        if (mSelected == null){
            mSelected = new Bundle();
        }
        if(mSelected.getBooleanArray(room.getName()) == null){
            boolean[] selectedPeriod = new boolean[Room.SIZE_OF_ADVANCED_INTERVALS];
            for (int i = 0; i < Room.SIZE_OF_ADVANCED_INTERVALS; i++){
                selectedPeriod[i] = false;
            }
            mSelected.putBooleanArray(room.getName(), selectedPeriod);
        }

        mAdvancedData.putBundle(Constants.SELECTED_DATA, mSelected);

        boolean[] selectedItemPeriod = mSelected.getBooleanArray(room.getName());

        int leftBoundary = -1;
        int rightBoundary = -1;
        for (int i = 0; i <= Room.SIZE_OF_ADVANCED_INTERVALS; i++){
            if(i != Room.SIZE_OF_ADVANCED_INTERVALS){
                if(leftBoundary == -1) {
                    if(selectedItemPeriod[i]) {
                        states[i].setBackgroundColor(getActivity().getResources().getColor(R.color.color_item_boundary));
                        leftBoundary = i;
                    }
                } else if(rightBoundary == -1) {
                    if(selectedItemPeriod[i]){
                        states[i].setBackgroundColor(getActivity().getResources().getColor(R.color.color_state_selected));
                    } else {
                        states[i - 1].setBackgroundColor(getActivity().getResources().getColor(R.color.color_item_boundary));
                        rightBoundary = i - 1;
                    }
                }
            } else {
                if(selectedItemPeriod[i - 1]){
                    rightBoundary = i - 1;
                    states[i - 1].setBackgroundColor(getActivity().getResources().getColor(R.color.color_item_boundary));
                }
            }
            if(i != Room.SIZE_OF_ADVANCED_INTERVALS){
                SetInterval.setIntervalListener(i, states, roomStatuses, selectedItemPeriod, getActivity(), Room.SIZE_OF_ADVANCED_INTERVALS);
            }
        }
    }

    @Override
    public Bundle getSharedDate() {
        Bundle data = new Bundle();
        data.putBundle(SHARED_DATA, mSharedData);
        return data;
    }

    class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Create a new instance of DatePickerDialog and return it
            DateTime date = mMeeting.getMeetingInterval().getStart();
            return new DatePickerDialog(getActivity(), this, date.getYear(), date.getMonthOfYear() - 1, date.getDayOfMonth());
        }

        @Override
        public void onDateSet(DatePicker view, int year, int month, int day) {
            DateTime startDate = mMeeting.getMeetingInterval().getStart().withDate(year, month + 1, day);
            DateTime endDate = mMeeting.getMeetingInterval().getEnd().withDate(year, month + 1, day);
            mMeeting.setMeetingInterval(new Interval(startDate, endDate));
            updateFragment();
        }
    }

    class FromTimePickerDialog extends DialogFragment implements TimePickerDialog.OnTimeSetListener{
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            DateTime startDate = mMeeting.getMeetingInterval().getStart();
            return new TimePickerDialog(getActivity(), this, startDate.getHourOfDay(), startDate.getMinuteOfHour(), true);
        }
        @Override
        public void onTimeSet(TimePicker timePicker, int hours, int minutes) {
            DateTime startDate = mMeeting.getMeetingInterval().getStart().withHourOfDay(hours).withMinuteOfHour(minutes);
            if(mMeeting.getMeetingInterval().getEnd().isBefore(startDate)){
                mMeeting.setMeetingInterval(new Interval(startDate, startDate));
            } else {
                mMeeting.getMeetingInterval().withStart(startDate);
            }
            mBtnFromTime.setText(startDate.toString(TIME_FORMAT));
            mBtnToTime.setText(mMeeting.getMeetingInterval().getEnd().toString(TIME_FORMAT));
        }
    }

    class ToTimePickerDialog extends DialogFragment implements TimePickerDialog.OnTimeSetListener{
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            DateTime endDate = mMeeting.getMeetingInterval().getEnd();
            return new TimePickerDialog(getActivity(), this, endDate.getHourOfDay(), endDate.getMinuteOfHour(), true);
        }
        @Override
        public void onTimeSet(TimePicker timePicker, int hours, int minutes) {
            DateTime startDate = mMeeting.getMeetingInterval().getStart();
            DateTime endDate = mMeeting.getMeetingInterval().getStart().withHourOfDay(hours).withMinuteOfHour(minutes);
            if(startDate.isAfter(endDate)){
                mMeeting.setMeetingInterval(new Interval(endDate, endDate));
                mBtnFromTime.setText(startDate.toString(TIME_FORMAT));
                mBtnToTime.setText(endDate.toString(TIME_FORMAT));
            } else {
                mMeeting.setMeetingInterval(new Interval(startDate, endDate));
                mBtnFromTime.setText(startDate.toString(TIME_FORMAT));
                mBtnToTime.setText(endDate.toString(TIME_FORMAT));
            }

        }
    }
}
