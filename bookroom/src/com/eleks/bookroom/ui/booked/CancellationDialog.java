package com.eleks.bookroom.ui.booked;

import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.eleks.bookroom.data.Constants;
import com.eleks.bookroom.R;
import com.eleks.bookroom.data.structures.Meeting;
import com.eleks.bookroom.ui.UpdatedFragment;

import java.util.concurrent.ExecutionException;
import static com.eleks.bookroom.data.Constants.*;

/**
 * Created by maryan.melnychuk on 18.12.13.
 */
public class CancellationDialog extends DialogFragment implements View.OnClickListener {
    private FragmentActivity mActivity;
    private Meeting mMeeting;
    private UpdatedFragment mUpdatedFragment;
    private TextView mTextViewLocation, mTextViewTimeFrom, mTextViewTimeTo, mTextDate, mTextTitle;
    private Button mBtnOk, mBtnCancel;

    public CancellationDialog(FragmentActivity activity, Meeting meeting, UpdatedFragment updatedFragment) {
        mActivity = activity;
        mMeeting = meeting;
        mUpdatedFragment = updatedFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.confirmation_dialog, parent);

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));

        mTextTitle = (TextView) view.findViewById(R.id.text_dialog_title);
        mTextViewLocation = (TextView) view.findViewById(R.id.dialog_confirmation_location);
        mTextViewTimeFrom = (TextView) view.findViewById(R.id.dialog_confirmation_from_time);
        mTextViewTimeTo = (TextView) view.findViewById(R.id.dialog_confirmation_to_time);
        mTextDate = (TextView) view.findViewById(R.id.dialog_confirmation_date);
        mBtnOk = (Button) view.findViewById(R.id.btn_dialog_ok);
        mBtnCancel = (Button) view.findViewById(R.id.btn_dialog_cancel);

        mTextTitle.setText(getActivity().getResources().getString(R.string.dialog_cancel_title));
        mTextViewLocation.setText(mActivity.getResources().getString(R.string.dialog_location) + " " + mMeeting.getLocation());
        mTextViewTimeFrom.setText(mActivity.getResources().getString(R.string.dialog_from) + " " + mMeeting.getMeetingInterval().getStart().toString(TIME_FORMAT));
        mTextViewTimeTo.setText(mActivity.getResources().getString(R.string.dialog_to) + " " + mMeeting.getMeetingInterval().getEnd().toString(TIME_FORMAT));
        mTextDate.setText(mActivity.getResources().getString(R.string.dialog_date) + " " + mMeeting.getMeetingInterval().getStart().toString(DATE_FORMAT));

        mBtnOk.setOnClickListener(this);
        mBtnCancel.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_dialog_ok:
                AsyncTask<Void, Void, Boolean> task = new AsyncTask<Void, Void, Boolean>() {
                    @Override
                    protected Boolean doInBackground(Void... voids) {
                        if(Constants.sExchangeHandler.cancelMeetingRequest(mMeeting)){
                            return true;
                        } else {
                            return false;
                        }
                    }
                };
                task.execute();
                try {
                    if(task.get()){
                        mUpdatedFragment.updateFragment();
                        getDialog().dismiss();
                        Toast.makeText(mActivity, R.string.meeting_canceled, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(mActivity, R.string.error_meeting_not_canceled, Toast.LENGTH_LONG).show();
                        if(!Constants.isNetworkOnline(getActivity())){
                            Toast.makeText(getActivity(), R.string.error_no_connection, Toast.LENGTH_LONG).show();
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.btn_dialog_cancel:
                getDialog().dismiss();
                break;
        }
    }
}
