package com.eleks.bookroom.ui.booked;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.eleks.bookroom.data.Constants;
import com.eleks.bookroom.R;
import com.eleks.bookroom.data.structures.Meeting;
import com.eleks.bookroom.ui.advanced.AdvancedBookingFragment;
import com.eleks.bookroom.ui.UpdatedFragment;

import java.util.ArrayList;
import java.util.List;
import static com.eleks.bookroom.data.Constants.*;

/**
 * Created by maryan.melnychuk on 16.12.13.
 */
public class MyBookedAdapter extends BaseAdapter {
    private Bundle mSharedData;
    private List<Meeting> mMeetings;
    private FragmentActivity mActivity;
    private LayoutInflater mInflater;
    private UpdatedFragment mUpdatedFragment;

    public MyBookedAdapter(FragmentActivity activity, List<Meeting> meetings, Bundle sharedData, UpdatedFragment updatedFragment) {
        mSharedData = sharedData;
        mMeetings = meetings;
        mActivity = activity;
        mInflater = activity.getLayoutInflater();
        mUpdatedFragment = updatedFragment;
    }

    @Override
    public int getCount() {
        return mMeetings.size();
    }

    @Override
    public Object getItem(int i) {
        return mMeetings.get(i);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View contentView, ViewGroup parent) {
        ViewHolder holder;
        View view = contentView;
        if(view == null){
            view =  mInflater.inflate(R.layout.booked_room_item, parent, false);
            holder = new ViewHolder();
            holder.name = (TextView) view.findViewById(R.id.my_booked_text_name);
            holder.startEvent = (TextView) view.findViewById(R.id.my_booked_event_start);
            holder.endEvent = (TextView) view.findViewById(R.id.my_booked_event_end);
            holder.dateEvent = (TextView) view.findViewById(R.id.my_booked_event_date);
            holder.btnEdit = (ImageButton) view.findViewById(R.id.btn_edit);
            holder.btnCancel = (ImageButton) view.findViewById(R.id.btn_cancel);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.name.setText(mMeetings.get(position).getLocation());
        holder.startEvent.setText(mActivity.getResources().getString(R.string.dialog_from) + " " + mMeetings.get(position).getMeetingInterval().getStart().toString(TIME_FORMAT));
        holder.endEvent.setText(mActivity.getResources().getString(R.string.dialog_to) + " " + mMeetings.get(position).getMeetingInterval().getEnd().toString(TIME_FORMAT));
        holder.dateEvent.setText(mActivity.getResources().getString(R.string.dialog_date) + " " + mMeetings.get(position).getMeetingInterval().getStart().toString(DATE_FORMAT));


        final int i = position;
        holder.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AdvancedBookingFragment fragment = new AdvancedBookingFragment(mMeetings.get(i), mSharedData);
                mActivity.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, fragment)
                        .commit();
            }
        });
        holder.btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CancellationDialog dialog = new CancellationDialog(mActivity, mMeetings.get(i), mUpdatedFragment);
                dialog.show(mActivity.getSupportFragmentManager(), "cancellation dialog");
            }
        });

        return view;
    }

    static class ViewHolder{
        public TextView name, startEvent, endEvent, dateEvent;
        public ImageButton btnEdit, btnCancel;
    }
}
