package com.eleks.bookroom.ui.login;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.bugsense.trace.BugSenseHandler;
import com.crashlytics.android.Crashlytics;
import com.eleks.bookroom.data.Constants;
import com.eleks.bookroom.data.databases.emails.EmailsDatabaseHelper;
import com.eleks.bookroom.data.exchange.ExchangeHandler;
import com.eleks.bookroom.data.databases.emails.GettingEmailsService;
import com.eleks.bookroom.R;
import com.eleks.bookroom.ui.main.MainActivity;

import java.util.concurrent.ExecutionException;

/**
 * Created by maryan.melnychuk on 11.12.13.
 */
public class LoginActivity extends ActionBarActivity implements View.OnClickListener {
    private SharedPreferences mPreferences;
    private String mDomain, mUsername, mPassword;
    private EditText mEditUsername, mEditPassword, mEditDomain;
    private CheckBox mCheckBoxSaveCredential;
    private Button mBtnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        new Thread(new Runnable() {
            @Override
            public void run() {
                EmailsDatabaseHelper db = new EmailsDatabaseHelper(LoginActivity.this);
                db.close();
            }
        });

        //bugsense integration
        if(Constants.IS_CRASH_ACTIVE){
            Crashlytics.start(this);
            BugSenseHandler.initAndStartSession(this, Constants.BUGSENSE_ID);
        }

        mPreferences = getSharedPreferences(Constants.PREFERENCES, MODE_PRIVATE);
        if(mPreferences.getBoolean(Constants.IS_PREFERENCES_SET, false)){
            mDomain = mPreferences.getString(Constants.PREFERENCES_DOMAIN, "");
            mUsername = mPreferences.getString(Constants.PREFERENCES_USERNAME, "");
            mPassword = mPreferences.getString(Constants.PREFERENCES_PASSWORD, "");
            Intent intent = new Intent(this, MainActivity.class);
            Constants.sExchangeHandler = new ExchangeHandler(mUsername, mPassword, mDomain, Constants.SERVER_URL);
            startActivity(intent);
            startService(new Intent(this, GettingEmailsService.class));

            finish();
        } else {
            setContentView(R.layout.login_activity);
            mEditUsername = (EditText) findViewById(R.id.edit_username);
            mEditPassword = (EditText) findViewById(R.id.edit_password);
            mEditDomain = (EditText) findViewById(R.id.edit_domain);
            mEditUsername.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View view, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                        onLoginClick();
                        return true;
                    }
                    return false;
                }
            });
            mEditPassword.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View view, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                        onLoginClick();
                        return true;
                    }
                    return false;
                }
            });
            mEditDomain.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View view, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                        onLoginClick();
                        return true;
                    }
                    return false;
                }
            });
            mCheckBoxSaveCredential = (CheckBox) findViewById(R.id.checkbox_save_credential);
            mBtnLogin = (Button) findViewById(R.id.btn_login);
            mBtnLogin.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_login:
                onLoginClick();
                break;
        }
    }

    private void onLoginClick(){
        if(mEditUsername.getText().toString().equals("")){
            Toast.makeText(this, R.string.error_domain_username_not_entered, Toast.LENGTH_LONG).show();
            return;
        }
        if(mEditPassword.getText().toString().equals("")){
            Toast.makeText(this, R.string.error_password_not_entered, Toast.LENGTH_LONG).show();
            return;
        }
        mDomain = mEditDomain.getText().toString();
        mUsername = mEditUsername.getText().toString();
        mPassword = mEditPassword.getText().toString();
        Constants.sExchangeHandler = new ExchangeHandler(mUsername, mPassword, mDomain, Constants.SERVER_URL);
        AsyncTask<Void, Void, Boolean> task = new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... voids) {
                return Constants.sExchangeHandler.checkConnection();
            }
        };
        task.execute();
        try {
            if(!task.get()) {
                if(Constants.isNetworkOnline(this)){
                    Toast.makeText(this, R.string.error_wrong_credential, Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(this, R.string.error_no_connection, Toast.LENGTH_LONG).show();
                }
                return;
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e){
            e.printStackTrace();
        }
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        if(mCheckBoxSaveCredential.isChecked()) {
            SharedPreferences.Editor editor = mPreferences.edit();
            editor.putString(Constants.PREFERENCES_DOMAIN, mDomain);
            editor.putString(Constants.PREFERENCES_USERNAME, mUsername);
            editor.putString(Constants.PREFERENCES_PASSWORD, mPassword);
            editor.putBoolean(Constants.IS_PREFERENCES_SET, true);
            editor.commit();
        }
        startService(new Intent(this, GettingEmailsService.class));
        finish();
    }
}
