package com.eleks.bookroom.data.databases.emails;

import android.app.Service;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.IBinder;
import android.provider.ContactsContract;
import android.util.Log;

import com.eleks.bookroom.data.Constants;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by maryan.melnychuk on 02.01.14.
 */
public class GettingEmailsService extends Service {
    private SharedPreferences sharedPreferences;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        sharedPreferences = GettingEmailsService.this.getSharedPreferences(Constants.PREFERENCES, MODE_PRIVATE);
        long currentTime = System.currentTimeMillis();
        long baseTime = sharedPreferences.getLong(Constants.PREFERENCES_BASE_DATE, currentTime);
        if(currentTime == baseTime){
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putLong(Constants.PREFERENCES_BASE_DATE, currentTime);
            editor.commit();
            addEmailsDb(getUsersEmail(new ArrayList<String>()));
        } else if(new DateTime(baseTime).plusWeeks(1).isBefore(currentTime) || baseTime == -1){
            getEmailsFromUserContactsAndExchange();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private List<String> getUsersEmail(List<String> userEmails){
        ContentResolver content = getContentResolver();
        Cursor emailCursor = content.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, null, null, null);
        while (emailCursor.moveToNext()) {
            String email = emailCursor.getString(emailCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
            if(!userEmails.contains(email)){
                userEmails.add(email);
            }
        }
        return userEmails;
    }

    private void addEmailsDb(final List<String> emails){
        Log.d(Constants.LOG_TAG, "addEmailsDb");
        EmailsDatabaseHelper emailsDatabaseHelper = new EmailsDatabaseHelper(GettingEmailsService.this);
        emailsDatabaseHelper.addEmails(emails);
        emailsDatabaseHelper.close();
    }

    private void getEmailsFromUserContactsAndExchange(){
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                List<String> userEmails = new ArrayList<String>();
                getUsersEmail(userEmails);
                try{
                    Constants.sExchangeHandler.getExchangeEmail(userEmails);
                } catch (Exception e){
                    Log.e(Constants.LOG_TAG, "getExchangeEmail problem");
                    e.printStackTrace();
                }
                Collections.sort(userEmails);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putLong(Constants.PREFERENCES_BASE_DATE, -1);
                EmailsDatabaseHelper emailsDatabaseHelper = new EmailsDatabaseHelper(GettingEmailsService.this);
                emailsDatabaseHelper.updateEmails(userEmails);
                emailsDatabaseHelper.close();
                editor.putLong(Constants.PREFERENCES_BASE_DATE, System.currentTimeMillis());
                editor.commit();
            }
        });
        thread.setPriority(Thread.MIN_PRIORITY);
        thread.start();
    }


}
