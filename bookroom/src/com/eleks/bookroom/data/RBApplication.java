package com.eleks.bookroom.data;

import android.app.Application;

import com.eleks.bookroom.data.serviceExecutor.RBServiceHandler;

/**
 * Created by maryan.melnychuk on 04.02.14.
 */
public class RBApplication extends Application {
    public static final String PACKAGE = "com.elelks.bookroom";

    private RBServiceHandler serviceHandler;
    private static RBApplication mApplication;

    @Override
    public void onCreate() {
        super.onCreate();
        serviceHandler = new RBServiceHandler(this);
        mApplication = this;
    }

    public RBServiceHandler getServiceHandler() {
        return serviceHandler;
    }

    public static RBApplication getApplication() {
        return mApplication;
    }
}
