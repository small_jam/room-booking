package com.eleks.bookroom.data.structures;

import android.os.Parcel;
import android.os.Parcelable;

import org.joda.time.DateTime;
import org.joda.time.Interval;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by maryan.melnychuk on 04.02.14.
 */
public class Room implements Parcelable {
    private String mName, mEmail;
    private List<DateTime> mDates = new ArrayList<DateTime>();
    private static Interval[] sIntervals;
    private List<List<Boolean>> mRoomStatus;

    public static final int SIZE_OF_DAY_INTERVALS = 96;
    public static final int SIZE_OF_QUICK_INTERVALS = 13;
    public static final int SIZE_OF_ADVANCED_INTERVALS = 60;
    private final static int INTERVALS_BEFORE_ADVANCED_INTERVAL = 28;
    private final static int INTERVALS_AFTER_ADVANCED_INTERVAL = 8;
    private final static int END_OF_ADVANCED_INTERVALS_ON_DAY_INTERVALS = SIZE_OF_DAY_INTERVALS - INTERVALS_AFTER_ADVANCED_INTERVAL;

    static {
        sIntervals = new Interval[SIZE_OF_DAY_INTERVALS];
        DateTime date = DateTime.now().withTime(0, 0, 0, 0);
        for (int i = 0; i < SIZE_OF_DAY_INTERVALS; i++){
            sIntervals[i] = new Interval(date.plusMinutes(i * 15), date.plusMinutes((i + 1) * 15).minus(1));
        }
    }

    public Room(){

    }

    public Room(String name, String email, List<DateTime> dates, List<List<Boolean>> roomStatus){
        mName = name;
        mEmail = email;
        mDates = dates;
        mRoomStatus = roomStatus;
    }

    public Room(Parcel parcel){
        mName = parcel.readString();
        mEmail = parcel.readString();
        int datesSize = parcel.readInt();
        long[] longDates = new long[datesSize];
        parcel.readLongArray(longDates);
        mDates = new ArrayList<DateTime>();
        for(int i = 0; i < datesSize; i++){
            mDates.add(new DateTime(longDates[i]));
        }
        mRoomStatus = new ArrayList<List<Boolean>>();
        for(int i = 0; i < datesSize; i++){
            boolean[] dateStatusArray = new boolean[SIZE_OF_DAY_INTERVALS];
            parcel.readBooleanArray(dateStatusArray);
            List<Boolean> dateStatus = new ArrayList<Boolean>();
            for(int j = 0; j < SIZE_OF_DAY_INTERVALS; j++){
                dateStatus.add(dateStatusArray[i]);
            }
            mRoomStatus.add(dateStatus);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flag) {
        parcel.writeString(mName);
        parcel.writeString(mEmail);
        int datesSize = mDates.size();
        parcel.writeInt(datesSize);
        long[] longDates = new long[datesSize];
        for (int i = 0; i < datesSize; i++){
            longDates[i] = mDates.get(i).getMillis();
        }
        parcel.writeLongArray(longDates);
        for(List<Boolean> dateStatus : mRoomStatus){
            boolean[] dateStatusArray = new boolean[SIZE_OF_DAY_INTERVALS];
            for (int i = 0; i < SIZE_OF_DAY_INTERVALS; i++){
                dateStatusArray[i] = dateStatus.get(i);
            }
            parcel.writeBooleanArray(dateStatusArray);
        }

    }

    public List<Boolean> getQuickRoomStatus(Interval interval){
        List<Boolean> quickRoomStatus = new ArrayList<Boolean>();
        if(interval.getStart().getDayOfMonth() == interval.getEnd().getDayOfMonth()){
            List<Boolean> statusList = getDayRoomStatus(interval.getStart());
            Interval[] actualInterval = getActualDayIntervals(interval.getStart());
            for (int i = 0; i < SIZE_OF_DAY_INTERVALS; i++){
                if(interval.overlap(actualInterval[i]) != null){
                    quickRoomStatus.add(statusList.get(i-1));
                }
            }
        } else {
            List<Boolean> statusStartList = getDayRoomStatus(interval.getStart());
            List<Boolean> statusEndList = getDayRoomStatus(interval.getEnd());
            Interval[] actualStartInterval = getActualDayIntervals(interval.getStart());
            Interval[] actualEndInterval = getActualDayIntervals(interval.getEnd());
            for (int i = 0; i < SIZE_OF_DAY_INTERVALS; i++){
                if(interval.overlap(actualStartInterval[i]) != null){
                    quickRoomStatus.add(statusStartList.get(i));
                }
            }
            for (int i = 0; i < SIZE_OF_DAY_INTERVALS; i++){
                if(interval.overlap(actualEndInterval[i]) != null){
                    quickRoomStatus.add(statusEndList.get(i));
                }
            }
        }
        return quickRoomStatus;
    }

    public List<Boolean> getAdvancedRoomStatus(DateTime day){
        List<Boolean> statusList = getDayRoomStatus(day);
        List<Boolean> advancedRoomStatus = new ArrayList<Boolean>();
        for(int i = INTERVALS_BEFORE_ADVANCED_INTERVAL - 1; i < END_OF_ADVANCED_INTERVALS_ON_DAY_INTERVALS; i++){
            advancedRoomStatus.add(statusList.get(i));
        }
        return advancedRoomStatus;
    }

    public List<Boolean> getDayRoomStatus(DateTime day){
        return mRoomStatus.get(mDates.indexOf(day.withTime(0, 0, 0, 0)));
    }

    public static Interval[] getActualQuickIntervals(Interval interval){
        Interval[] quickIntervals = new Interval[SIZE_OF_QUICK_INTERVALS];
        if(interval.getStart().getDayOfMonth() == interval.getEnd().getDayOfMonth()){
            Interval[] actualDayIntervals = getActualDayIntervals(interval.getStart());
            int j = 0;
            for(int i = 0; i < SIZE_OF_DAY_INTERVALS; i++){
                 if(actualDayIntervals[i].overlap(interval) != null){
                     quickIntervals[j] = actualDayIntervals[i];
                     j++;
                 }
            }
        } else {
            Interval[] actualStartDayIntervals = getActualDayIntervals(interval.getStart());
            Interval[] actualEndDayIntervals = getActualDayIntervals(interval.getEnd());
            int j = 0;
            for(int i = 0; i < SIZE_OF_DAY_INTERVALS; i++){
                if(actualStartDayIntervals[i].overlap(interval) != null){
                    quickIntervals[j] = actualStartDayIntervals[i];
                    j++;
                }
            }
            for(int i = 0; i < SIZE_OF_DAY_INTERVALS; i++){
                if(actualEndDayIntervals[i].overlap(interval) != null){
                    quickIntervals[j] = actualEndDayIntervals[i];
                    j++;
                }
            }
        }
        return quickIntervals;
    }

    public static Interval[] getActualAdvancedIntervals(DateTime day){
        Interval[] dayInterval = getActualDayIntervals(day);
        Interval[] advancedInterval = new Interval[SIZE_OF_ADVANCED_INTERVALS];
        int j = 0;
        for (int i = INTERVALS_BEFORE_ADVANCED_INTERVAL; i < END_OF_ADVANCED_INTERVALS_ON_DAY_INTERVALS; i++){
            advancedInterval[j] = dayInterval[i];
            j++;
        }
        return advancedInterval;
    }

    public static Interval[] getActualDayIntervals(DateTime day){
        Interval[] actualInterval = new Interval[SIZE_OF_DAY_INTERVALS];
        for (int i = 0; i < SIZE_OF_DAY_INTERVALS; i++){
            DateTime start = sIntervals[i].getStart().withDate(day.getYear(), day.getMonthOfYear(), day.getDayOfMonth());
            DateTime end = sIntervals[i].getEnd().withDate(day.getYear(), day.getMonthOfYear(), day.getDayOfMonth());
            Interval interval = new Interval(start, end);
            actualInterval[i] = interval;
        }
        return actualInterval;
    }

    public static DateTime[] getActualBoundaryQuickIntervals(Interval[] intervals){
        DateTime[] boundary = new DateTime[SIZE_OF_QUICK_INTERVALS + 1];
        for (int i = 0; i < SIZE_OF_QUICK_INTERVALS; i++){
            boundary[i] = intervals[i].getStart();
            if(i == SIZE_OF_QUICK_INTERVALS - 1){
                boundary[i + 1] = intervals[i].getEnd().plusMinutes(1);
            }
        }
        return boundary;
    }

    public static DateTime[] getActualBoundaryAdvancedIntervals(Interval[] intervals){
        DateTime[] boundary = new DateTime[SIZE_OF_ADVANCED_INTERVALS + 1];
        for (int i = 0; i < SIZE_OF_ADVANCED_INTERVALS; i++){
            boundary[i] = intervals[i].getStart();
            if(i == SIZE_OF_ADVANCED_INTERVALS - 1){
                boundary[i + 1] = intervals[i].getEnd().plusMinutes(1);
            }
        }
        return boundary;
    }

    public static Interval getActualQuickInterval(){
        DateTime startTime = DateTime.now().withSecondOfMinute(0).withMillisOfSecond(0);
        int actualMinutes = startTime.getMinuteOfHour();
        if(actualMinutes < 15){
            startTime = startTime.withMinuteOfHour(0);
        } else if (actualMinutes < 30){
            startTime = startTime.withMinuteOfHour(15);
        } else if (actualMinutes < 45){
            startTime = startTime.withMinuteOfHour(30);
        } else {
            startTime = startTime.withMinuteOfHour(45);
        }
        DateTime endTime = startTime.plusHours(3).plusMinutes(15).minus(1);

        return new Interval(startTime, endTime);
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String mEmail) {
        this.mEmail = mEmail;
    }

    public List<DateTime> getDates() {
        return mDates;
    }

    public void setDates(List<DateTime> mDates) {
        this.mDates = mDates;
    }

    public List<List<Boolean>> getRoomStatus() {
        return mRoomStatus;
    }

    public void setRoomStatus(List<List<Boolean>> mRoomStatus) {
        this.mRoomStatus = mRoomStatus;
    }

    public static final Parcelable.Creator<Room> CREATOR = new Parcelable.Creator<Room>() {
        public Room createFromParcel(Parcel parcel) {
            return new Room(parcel);
        }

        public Room[] newArray(int size) {
            return new Room[size];
        }
    };
}
