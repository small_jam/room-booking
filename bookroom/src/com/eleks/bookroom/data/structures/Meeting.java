package com.eleks.bookroom.data.structures;

import android.os.Parcel;
import android.os.Parcelable;

import org.joda.time.DateTime;
import org.joda.time.Interval;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by maryan.melnychuk on 04.02.14.
 */
public class Meeting implements Parcelable {
    private String mSubject, mUniqueId, mMessageOfBody, mLocation, mLocationEmail;
    private List<String> mInvites = new ArrayList<String>();
    private Interval mMeetingInterval;

    public Meeting(){

    }

    public Meeting(Parcel parcel){
        mSubject = parcel.readString();
        mUniqueId = parcel.readString();
        mMessageOfBody = parcel.readString();
        mLocation = parcel.readString();
        mLocationEmail = parcel.readString();
        mInvites = new ArrayList<String>();
        parcel.readStringList(mInvites);
        long startInterval = parcel.readLong();
        long endInterval = parcel.readLong();
        mMeetingInterval = new Interval(new DateTime(startInterval), new DateTime(endInterval));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(mSubject);
        parcel.writeString(mUniqueId);
        parcel.writeString(mMessageOfBody);
        parcel.writeString(mLocation);
        parcel.writeString(mLocationEmail);
        int invitesSize = mInvites.size();
        parcel.writeStringList(mInvites);
        parcel.writeInt(invitesSize);
        parcel.writeLong(mMeetingInterval.getStart().getMillis());
        parcel.writeLong(mMeetingInterval.getEnd().getMillis());
    }

    public Interval getMeetingInterval() {
        return mMeetingInterval;
    }

    public void setMeetingInterval(Interval mMeetingInterval) {
        this.mMeetingInterval = mMeetingInterval;
    }

    public String getSubject() {
        return mSubject;
    }

    public void setSubject(String mSubject) {
        this.mSubject = mSubject;
    }

    public String getUniqueId() {
        return mUniqueId;
    }

    public void setUniqueId(String mUniqueId) {
        this.mUniqueId = mUniqueId;
    }

    public String getMessageOfBody() {
        return mMessageOfBody;
    }

    public void setMessageOfBody(String mMessageOfBody) {
        this.mMessageOfBody = mMessageOfBody;
    }

    public String getLocation() {
        return mLocation;
    }

    public void setLocation(String mLocation) {
        this.mLocation = mLocation;
    }

    public List<String> getInvites() {
        return mInvites;
    }

    public void setInvites(List<String> mInvites) {
        this.mInvites = mInvites;
    }

    public String getLocationEmail() {
        return mLocationEmail;
    }

    public void setLocationEmail(String mLocationEmail) {
        this.mLocationEmail = mLocationEmail;
    }

    public static final Parcelable.Creator<Meeting> CREATOR = new Parcelable.Creator<Meeting>() {
        public Meeting createFromParcel(Parcel parcel) {
            return new Meeting(parcel);
        }

        public Meeting[] newArray(int size) {
            return new Meeting[size];
        }
    };


}
