package com.eleks.bookroom.data.serviceExecutor;

import android.app.Service;
import android.content.Intent;
import android.os.*;
import android.os.Process;
import android.util.Log;
import android.util.SparseArray;

import com.eleks.bookroom.data.Constants;
import com.eleks.bookroom.data.RBApplication;
import com.eleks.bookroom.data.commands.RBBaseCommand;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by maryan.melnychuk on 05.02.14.
 */
public class RBCommandExecutorService extends Service {

    private static final int NUM_THREADS = 10;

    public static final String ACTION_EXECUTE_COMMAND = RBApplication.PACKAGE.concat(".ACTION_EXECUTE_COMMAND");

    public static final String ACTION_CANCEL_COMMAND = RBApplication.PACKAGE.concat(".ACTION_CANCEL_COMMAND");

    public static final String EXTRA_REQUEST_ID = RBApplication.PACKAGE.concat(".EXTRA_REQUEST_ID");

    public static final String EXTRA_STATUS_RECEIVER = RBApplication.PACKAGE.concat(".STATUS_RECEIVER");

    public static final String EXTRA_COMMAND = RBApplication.PACKAGE.concat(".EXTRA_COMMAND");

    private ExecutorService executor = Executors.newFixedThreadPool(NUM_THREADS);

    private SparseArray<RunningCommand> runningCommands = new SparseArray<RunningCommand>();

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        Log.d(Constants.LOG_TAG, "onDestroy");
        super.onDestroy();

        executor.shutdownNow();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(Constants.LOG_TAG, "onStartCommand");
        if (ACTION_EXECUTE_COMMAND.equals(intent.getAction())) {
            RunningCommand runningCommand = new RunningCommand(intent);

            synchronized (runningCommands) {
                runningCommands.append(getCommandId(intent), runningCommand);
            }

            executor.submit(runningCommand);
        }
        if (ACTION_CANCEL_COMMAND.equals(intent.getAction())) {
            RunningCommand runningCommand = runningCommands.get(getCommandId(intent));
            if (runningCommand != null) {
                runningCommand.cancel();
            }
        }

        return START_NOT_STICKY;
    }

    private class RunningCommand implements Runnable {

        private Intent intent;

        private RBBaseCommand command;

        public RunningCommand(Intent intent) {
            Log.d(Constants.LOG_TAG, "RunningCommand");
            this.intent = intent;

            command = getCommand(intent);
        }

        public void cancel() {
            command.cancel();
        }

        @Override
        public void run() {
            Log.d(Constants.LOG_TAG, "run");
            android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
            command.execute(intent, getApplicationContext(), getReceiver(intent));

            shutdown();
        }

        private void shutdown() {
            Log.d(Constants.LOG_TAG, "shutdown");
            synchronized (runningCommands) {
                runningCommands.remove(getCommandId(intent));
                if (runningCommands.size() == 0) {
                    stopSelf();
                }
            }
        }

    }

    private ResultReceiver getReceiver(Intent intent) {
        return intent.getParcelableExtra(EXTRA_STATUS_RECEIVER);
    }

    private RBBaseCommand getCommand(Intent intent) {
        return intent.getParcelableExtra(EXTRA_COMMAND);
    }

    private int getCommandId(Intent intent) {
        return intent.getIntExtra(EXTRA_REQUEST_ID, -1);
    }
}
