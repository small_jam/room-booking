package com.eleks.bookroom.data.serviceExecutor;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.provider.ContactsContract;
import android.util.Log;
import android.util.SparseArray;

import com.eleks.bookroom.data.Constants;
import com.eleks.bookroom.data.commands.GetMeetingCommand;
import com.eleks.bookroom.data.commands.GetRoomStatusCommand;
import com.eleks.bookroom.data.commands.GetUserMeetingsCommand;
import com.eleks.bookroom.data.commands.RBBaseCommand;
import com.eleks.bookroom.data.serviceExecutor.RBCommandExecutorService;
import com.eleks.bookroom.data.serviceExecutor.RBServiceCallbackListener;
import com.eleks.bookroom.data.structures.Meeting;

import org.joda.time.Interval;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by maryan.melnychuk on 04.02.14.
 */
public class RBServiceHandler {
    private ArrayList<RBServiceCallbackListener> mCurrentListeners = new ArrayList<RBServiceCallbackListener>();
    private AtomicInteger mIdCounter = new AtomicInteger();
    private SparseArray<Intent> mPendingActivities = new SparseArray<Intent>();
    private Application mApplication;

    public RBServiceHandler(Application app){
        Log.d(Constants.LOG_TAG, "RBServiceHandler");
        mApplication = app;
    }

    public void addListener(RBServiceCallbackListener currentListener) {
        Log.d(Constants.LOG_TAG, "addListener");
        mCurrentListeners.add(currentListener);
    }

    public void removeListener(RBServiceCallbackListener currentListener) {
        Log.d(Constants.LOG_TAG, "removeListener");
        mCurrentListeners.remove(currentListener);
    }

    //Commands

    public int getRoomStatus(Interval interval){
        Log.d(Constants.LOG_TAG, "getRoomStatus");
        final int requestId = createId();

        Intent intent = createIntent(mApplication, new GetRoomStatusCommand(interval), requestId);
        return runRequest(requestId, intent);
    }

    public int getMeeting(Meeting meeting){
        Log.d(Constants.LOG_TAG, "getMeeting");
        final int requestId = createId();

        Intent intent = createIntent(mApplication, new GetMeetingCommand(meeting), requestId);
        return runRequest(requestId, intent);
    }

    public int getUserMeetings(int duration){
        Log.d(Constants.LOG_TAG, "getUserMeetings");
        final int requestId = createId();

        Intent intent = createIntent(mApplication, new GetUserMeetingsCommand(duration), requestId);
        return runRequest(requestId, intent);
    }

    //======================================================================================================================

    public void cancelCommand(int requestId){
        Log.d(Constants.LOG_TAG, "cancelCommand");
        Intent intent = new Intent(mApplication, RBCommandExecutorService.class);
        intent.setAction(RBCommandExecutorService.ACTION_CANCEL_COMMAND);
        intent.putExtra(RBCommandExecutorService.EXTRA_REQUEST_ID, requestId);

        mApplication.startService(intent);
        mPendingActivities.remove(requestId);
    }

    public boolean isPending(int requestCode){
        Log.d(Constants.LOG_TAG, "isPending");
        return  mPendingActivities.get(requestCode) != null;
    }

    public boolean check(Intent intent, Class<? extends RBBaseCommand> clazz){
        Log.d(Constants.LOG_TAG, "check");
        Parcelable commandExtra = intent.getParcelableExtra(RBCommandExecutorService.EXTRA_COMMAND);
        return commandExtra != null && commandExtra.getClass().equals(clazz);
    }

    private int createId(){
        Log.d(Constants.LOG_TAG, "createId");
        return mIdCounter.getAndIncrement();
    }

    private int runRequest(final int requestId, Intent intent){
        Log.d(Constants.LOG_TAG, "runRequest");
        mPendingActivities.append(requestId, intent);
        mApplication.startService(intent);
        return requestId;
    }

    private Intent createIntent(final Context context, RBBaseCommand command, final int requestId){
        Log.d(Constants.LOG_TAG, "createIntent");
        Intent intent = new Intent(context, RBCommandExecutorService.class);
        intent.setAction(RBCommandExecutorService.ACTION_EXECUTE_COMMAND);

        intent.putExtra(RBCommandExecutorService.EXTRA_COMMAND, command);
        intent.putExtra(RBCommandExecutorService.EXTRA_REQUEST_ID, requestId);
        intent.putExtra(RBCommandExecutorService.EXTRA_STATUS_RECEIVER, new ResultReceiver(new Handler()){
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
                Intent originalIntent = mPendingActivities.get(requestId);
                if(isPending(requestId)){
                    if(resultCode != RBBaseCommand.RESPONSE_PROGRESS){
                        mPendingActivities.remove(requestId);
                    }

                    for (RBServiceCallbackListener currentListener : mCurrentListeners){
                        if(currentListener != null){
                            currentListener.onServiceCallback(requestId, originalIntent, resultCode, resultData);
                        }
                    }
                }
            }
        });

        return intent;
    }

}
