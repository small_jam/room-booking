package com.eleks.bookroom.data;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.eleks.bookroom.data.exchange.ExchangeHandler;

/**
 * Created by maryan.melnychuk on 05.12.13.
 */
public final class Constants {
    public static final String BUGSENSE_ID = "ab769ac2";
    public static final boolean IS_CRASH_ACTIVE = true;
    public static final boolean EWS_TRACE_ENABLED = false;

    public static final String LOG_TAG = "debug_book";

    public static final String SERVER_URL = "https://webmail.eleks.com/EWS/Exchange.asmx";


    public static final String TIME_FORMAT = "HH:mm";
    public static final String DATE_FORMAT = "dd-MM-yyyy";


    public static ExchangeHandler sExchangeHandler;
    //public static List<String> sUserContactsEmail;

    //preferences
    public static final String PREFERENCES = "preferences_book_room";
    public static final String IS_PREFERENCES_SET = "is_preferences";
    public static final String PREFERENCES_DOMAIN = "preferences_domain";
    public static final String PREFERENCES_USERNAME = "preferences_username";
    public static final String PREFERENCES_PASSWORD = "preferences_password";
    public static final String PREFERENCES_BASE_DATE = "preferences_base_date";

    //Bundle data
    public static final String QUICK_FREE_BUSY_DATA = "quick_free_busy_data";
    public static final String ADVANCED_FREE_BUSY_DATA = "advanced_free_busy_data";
    public static final String MEETING = "meetings";
    public static final String SHARED_DATA = "shared_data";
    public static final String TYPE_ADVANCED = "advanced_type";
    public static final String SELECTED = "selected";
    public static final String ACTIVE_ROOM = "active_room";
    public static final String SELECTED_DATA = "selected_data";

    //command data
    public static final String ROOM_LIST = "room_list";
    public static final String MEETING_LIST = "meeting_list";
    public static final String ERROR = "error";

    public static final String CURRENT_DURATION = "current_duration";
    public static final int DURATION_DAY = 1;
    public static final int DURATION_WEEK = 2;
    public static final int DURATION_MONTH = 3;
    public static final int DURATION_TWO_MONTH = 4;

    public static boolean isNetworkOnline(Context context) {
        boolean status=false;
        try{
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getNetworkInfo(0);
            if (netInfo != null && netInfo.getState()==NetworkInfo.State.CONNECTED) {
                status= true;
            }else {
                netInfo = cm.getNetworkInfo(1);
                if(netInfo!=null && netInfo.getState()==NetworkInfo.State.CONNECTED)
                    status= true;
            }
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
        return status;

    }

}
