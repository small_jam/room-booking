package com.eleks.bookroom.data.commands;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.ResultReceiver;

import com.eleks.bookroom.data.structures.Meeting;

import static com.eleks.bookroom.data.Constants.ERROR;
import static com.eleks.bookroom.data.Constants.MEETING;
import static com.eleks.bookroom.data.Constants.sExchangeHandler;

/**
 * Created by maryan.melnychuk on 06.02.14.
 */
public class GetMeetingCommand extends RBBaseCommand {
    Meeting mMeeting;

    public GetMeetingCommand(Meeting meeting) {
        mMeeting = meeting;
    }

    public  GetMeetingCommand(Parcel parcel){
        mMeeting = parcel.readParcelable(Meeting.class.getClassLoader());
    }

    @Override
    protected void doExecute(Intent intent, Context context, ResultReceiver callback) {
        Bundle data = new Bundle();
        try{
            mMeeting = sExchangeHandler.getMeeting(mMeeting);
        } catch (Exception e){
            e.printStackTrace();
            data.putString(ERROR, e.toString());
            notifyFailure(data);
        }


        if(mMeeting != null){
            data.putParcelable(MEETING, mMeeting);
            notifySuccess(data);
        } else {
            data.putString(ERROR, "Meeting is null");
            notifyFailure(data);
        }

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(mMeeting, Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
    }

    public static final Parcelable.Creator<GetMeetingCommand> CREATOR = new Parcelable.Creator<GetMeetingCommand>() {
        public GetMeetingCommand createFromParcel(Parcel parcel) {
            return new GetMeetingCommand(parcel);
        }

        public GetMeetingCommand[] newArray(int size) {
            return new GetMeetingCommand[size];
        }
    };

}
