package com.eleks.bookroom.data.commands;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.ResultReceiver;

import com.eleks.bookroom.data.structures.Meeting;

import java.util.ArrayList;
import java.util.List;

import static com.eleks.bookroom.data.Constants.ERROR;
import static com.eleks.bookroom.data.Constants.MEETING_LIST;
import static com.eleks.bookroom.data.Constants.sExchangeHandler;

/**
 * Created by maryan.melnychuk on 07.02.14.
 */
public class GetUserMeetingsCommand extends RBBaseCommand {
    private int mDuration;

    public GetUserMeetingsCommand(int duration){
        mDuration = duration;
    }

    public GetUserMeetingsCommand(Parcel parcel){
        mDuration = parcel.readInt();
    }

    @Override
    protected void doExecute(Intent intent, Context context, ResultReceiver callback) {
        Bundle data = new Bundle();
        List<Meeting> meetings = null;
        try{
            meetings = sExchangeHandler.getUserMeetingsList(mDuration);
        } catch (Exception e){
            e.printStackTrace();
            data.putString(ERROR, e.toString());
            notifyFailure(data);
        }
        if(meetings != null){
            data.putParcelableArrayList(MEETING_LIST, (ArrayList) meetings);
            notifySuccess(data);
        } else {
            data.putString(ERROR, "Meetings are null");
            notifyFailure(data);
        }

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(mDuration);
    }

    public static final Parcelable.Creator<GetUserMeetingsCommand> CREATOR = new Parcelable.Creator<GetUserMeetingsCommand>() {
        public GetUserMeetingsCommand createFromParcel(Parcel parcel) {
            return new GetUserMeetingsCommand(parcel);
        }

        public GetUserMeetingsCommand[] newArray(int size) {
            return new GetUserMeetingsCommand[size];
        }
    };
}
