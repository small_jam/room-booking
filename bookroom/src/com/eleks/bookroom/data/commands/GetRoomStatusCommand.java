package com.eleks.bookroom.data.commands;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.ResultReceiver;

import com.eleks.bookroom.data.structures.Room;

import org.joda.time.DateTime;
import org.joda.time.Interval;

import java.util.ArrayList;
import java.util.List;

import static com.eleks.bookroom.data.Constants.*;
/**
 * Created by maryan.melnychuk on 05.02.14.
 */
public class GetRoomStatusCommand extends RBBaseCommand {
    private static final String TAG = "GetRoomStatusCommand";
    private Interval mInterval;

    public GetRoomStatusCommand(Interval interval){
        mInterval = interval;
    }

    public GetRoomStatusCommand(Parcel parcel){
        DateTime start = new DateTime(parcel.readLong());
        DateTime end = new DateTime(parcel.readLong());
        mInterval = new Interval(start, end);
    }

    @Override
    protected void doExecute(Intent intent, Context context, ResultReceiver callback) {
        Bundle data = new Bundle();
        List<Room> rooms = null;
        try{
            rooms = sExchangeHandler.getRooms(mInterval);
        } catch (Exception e){
            e.printStackTrace();
            data.putString(ERROR, e.toString());
            notifyFailure(data);
        }
        if(rooms != null){
            data.putParcelableArrayList(ROOM_LIST, (ArrayList)rooms);
            notifySuccess(data);
        } else {
            data.putString(ERROR, "Rooms are null");
            notifyFailure(data);
        }

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flag) {
        parcel.writeLong(mInterval.getStart().getMillis());
        parcel.writeLong(mInterval.getEnd().getMillis());
    }

    public static final Parcelable.Creator<GetRoomStatusCommand> CREATOR = new Parcelable.Creator<GetRoomStatusCommand>() {
        public GetRoomStatusCommand createFromParcel(Parcel parcel) {
            return new GetRoomStatusCommand(parcel);
        }

        public GetRoomStatusCommand[] newArray(int size) {
            return new GetRoomStatusCommand[size];
        }
    };
}
