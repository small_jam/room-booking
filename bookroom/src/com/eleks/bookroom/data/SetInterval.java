package com.eleks.bookroom.data;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.eleks.bookroom.R;

import java.util.List;

/**
 * Created by maryan.melnychuk on 04.01.14.
 */

public class SetInterval {
    public static void setIntervalListener(final int j, final View[] states, final List<Boolean> roomStatuses, final boolean[] selectedItemPeriod, final Context context, final int mode){
        states[j].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (roomStatuses.get(j)) {
                    int[] boundaries = getBoundary(roomStatuses, selectedItemPeriod, mode);
                    final int leftBoundary = boundaries[0];
                    final int rightBoundary = boundaries[1];
                    if(leftBoundary != -1 && rightBoundary != -1){
                        hasBothBoundary(j, leftBoundary, rightBoundary, context, states, selectedItemPeriod, roomStatuses, mode);
                    } else if(leftBoundary != -1){
                        hasLeftBoundary(j, leftBoundary, context, states, selectedItemPeriod, roomStatuses);
                    } else {
                        setSelected(context, j, states, selectedItemPeriod, true);
                    }
                }
            }
        });
    }

    private static void hasBothBoundary(int j, int leftBoundary, int rightBoundary, Context context, View[] states, boolean[] selectedItemPeriod,
                                        List<Boolean> roomStatuses, int mode){
        if(j < leftBoundary){
            smallerThenLeftBoundaryHasBothBoundary(j, leftBoundary, rightBoundary, context, states, selectedItemPeriod, roomStatuses, mode);
        } else if(j > rightBoundary){
            biggerThanRightBoundaryHasBothBoundary(j, leftBoundary, rightBoundary, context, states, selectedItemPeriod, roomStatuses);
        } else if(rightBoundary == leftBoundary + 1){
            if(j == leftBoundary){
                equalWithLeftBoundaryAndBoundaryAreNearHasBothBoundary(leftBoundary, rightBoundary, context, states, selectedItemPeriod, roomStatuses);
            } else {
                equalWithRightBoundaryAndBoundaryAreNearHasBothBoundary(leftBoundary, rightBoundary, context, states, selectedItemPeriod, roomStatuses);
            }
        } else if(j > leftBoundary && j < rightBoundary) {
            betweenBoundariesHasBothBoundary(j, leftBoundary, rightBoundary, context, states, selectedItemPeriod);
        }
    }

    private static void smallerThenLeftBoundaryHasBothBoundary(int j, int leftBoundary, int rightBoundary, Context context, View[] states,
                                                               boolean[] selectedItemPeriod, List<Boolean> roomStatuses, int mode){
        for (int i = leftBoundary - 1; i > j; i--){
            if(!roomStatuses.get(i)){
                setSelected(context, i + 1, states, selectedItemPeriod, true);
                return;
            }
            setSelected(context, i, states, selectedItemPeriod, false);
        }
        if(rightBoundary - leftBoundary == 1){
            if(leftBoundary == mode - 1){
                setSelected(context, leftBoundary, states, selectedItemPeriod, true);
            } else {
                setSelected(context, leftBoundary, states, selectedItemPeriod, false);
            }
        } else {
            if(leftBoundary != rightBoundary){
                setSelected(context, leftBoundary, states, selectedItemPeriod, false);
            } else {
                setSelected(context, leftBoundary, states, selectedItemPeriod, true);
            }
        }
        setSelected(context, j, states, selectedItemPeriod, true);
    }
    private static void biggerThanRightBoundaryHasBothBoundary(int j, int leftBoundary, int rightBoundary, Context context, View[] states,
                                                               boolean[] selectedItemPeriod, List<Boolean> roomStatuses){
        for (int i = leftBoundary + 1; i < j; i++){
            if(!roomStatuses.get(i)){
                setSelected(context, i - 1, states, selectedItemPeriod, true);
                return;
            }
            setSelected(context, i, states, selectedItemPeriod, false);
        }
        setSelected(context, j, states, selectedItemPeriod, true);
        setSelected(context, rightBoundary, states, selectedItemPeriod, false);
    }
    private static void equalWithLeftBoundaryAndBoundaryAreNearHasBothBoundary(int leftBoundary, int rightBoundary, Context context, View[] states,
                                                                               boolean[] selectedItemPeriod, List<Boolean> roomStatuses){
        if(roomStatuses.get(rightBoundary)){
            setAvailable(context, rightBoundary, states, selectedItemPeriod);
        } else {
            setAvailable(context, leftBoundary, states, selectedItemPeriod);
        }
    }
    private static void equalWithRightBoundaryAndBoundaryAreNearHasBothBoundary(int leftBoundary, int rightBoundary, Context context, View[] states,
                                                                                boolean[] selectedItemPeriod, List<Boolean> roomStatuses){
        if(roomStatuses.get(leftBoundary)){
            setAvailable(context, leftBoundary, states, selectedItemPeriod);
        } else {
            setAvailable(context, rightBoundary, states, selectedItemPeriod);
        }
    }
    private static void betweenBoundariesHasBothBoundary(int j, int leftBoundary, int rightBoundary, Context context, View[] states,
                                                         boolean[] selectedItemPeriod){
        int averageLeft = j - leftBoundary;
        int averageRight = rightBoundary - j;
        if(averageLeft < averageRight){
            for (int i = j; i >= leftBoundary; i--){
                setAvailable(context, i, states, selectedItemPeriod);
            }
            setSelected(context, j, states, selectedItemPeriod, true);
        } else {
            for (int i = j; i <= rightBoundary; i++){
                setAvailable(context, i, states, selectedItemPeriod);
            }
            setSelected(context, j, states, selectedItemPeriod, true);
        }
    }

    private static void hasLeftBoundary(int j, int leftBoundary, Context context, View[] states, boolean[] selectedItemPeriod, List<Boolean> roomStatuses){
        if(j < leftBoundary){
            smallerThenLeftBoundaryHasLeftBoundary(j, leftBoundary, context, states, selectedItemPeriod, roomStatuses);
        } else if(j > leftBoundary){
            biggerThenLeftBoundaryHasLeftBoundary(j, leftBoundary, context, states, selectedItemPeriod, roomStatuses);
        } else if(j == leftBoundary) {
            setAvailable(context, j, states, selectedItemPeriod);
        }
    }

    private static void smallerThenLeftBoundaryHasLeftBoundary(int j, int leftBoundary, Context context, View[] states, boolean[] selectedItemPeriod,
                                                               List<Boolean> roomStatuses){
        for (int i = leftBoundary - 1; i > j; i--){
            if(!roomStatuses.get(i)){
                setSelected(context, i + 1, states, selectedItemPeriod, true);
                return;
            }
            setSelected(context, i, states, selectedItemPeriod, false);
        }
        setSelected(context, j, states, selectedItemPeriod, true);
    }
    private static void biggerThenLeftBoundaryHasLeftBoundary(int j, int leftBoundary, Context context, View[] states, boolean[] selectedItemPeriod,
                                                              List<Boolean> roomStatuses){
        for (int i = leftBoundary + 1; i < j; i++){
            if(!roomStatuses.get(i)){
                setSelected(context, i - 1, states, selectedItemPeriod, true);
                return;
            }
            setSelected(context, i, states, selectedItemPeriod, false);
        }
        setSelected(context, j, states, selectedItemPeriod, true);
    }

    private static void setAvailable(Context context, int i, View[] states, boolean[] selectedItemPeriod){
        selectedItemPeriod[i] = false;
        states[i].setBackgroundColor(context.getResources().getColor(R.color.color_state_available));
    }
    private static void setSelected(Context context, int i, View[] states, boolean[] selectedItemPeriod, boolean isBoundary){
        selectedItemPeriod[i] = true;
        if(isBoundary){
            states[i].setBackgroundColor(context.getResources().getColor(R.color.color_item_boundary));
        } else {
            states[i].setBackgroundColor(context.getResources().getColor(R.color.color_state_selected));
        }
    }

    private static int[] getBoundary(List<Boolean> roomStatuses, boolean[] selectedItemPeriod, int mode){
        int leftBoundary = -1;
        int rightBoundary = -1;
        for (int i = 0; i <= mode; i++){
            if(i != mode){
                if(leftBoundary == -1) {
                    if(selectedItemPeriod[i]) {
                        leftBoundary = i;
                    }
                } else if(rightBoundary == -1) {
                    if(roomStatuses.get(i)){
                        if(!selectedItemPeriod[i]){
                            if(leftBoundary != i - 1){
                                rightBoundary = i - 1;
                            } else {
                                break;
                            }
                        }
                    } else {
                        if(leftBoundary != i - 1){
                            rightBoundary = i - 1;
                        }
                    }
                }
            } else {
                if(selectedItemPeriod[i - 1]){
                    rightBoundary = i - 1;
                }
            }
        }
        return new int[]{leftBoundary, rightBoundary};
    }
}
