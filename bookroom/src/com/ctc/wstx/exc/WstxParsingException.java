package com.ctc.wstx.exc;

import eleks.javax.xml.stream.Location;
import eleks.javax.xml.stream.XMLStreamException;

/**
 * Intermediate base class for reporting actual Wstx parsing problems.
 */
public class WstxParsingException
    extends WstxException
{
    public WstxParsingException(String msg, Location loc) {
        super(msg, loc);
    }
}
